import axios from "axios";

// Create an Axios instance
const instance = axios.create({
  baseURL: "http://127.0.0.1:8000/",
});

// Set up request interceptors
instance.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("jwtToken");
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// Set up response interceptors
instance.interceptors.response.use(
  (response) => {
    // Do something with the response data
    return response;
  },
  (error) => {
    if (error.response && error.response.status === 401) {
      localStorage.removeItem("jwtToken"); // Remove the expired or invalid token

      // Redirect to login page using window.location
      window.location = "/login";
    }
    return Promise.reject(error);
  }
);

export default instance;
