export function createCustomReporter() {
  if (!window.mocha || !window.mocha.reporters) {
    throw new Error("Mocha is not loaded");
  }

  class CustomReporter extends window.mocha.reporters.Base {
    constructor(runner) {
      super(runner);

      runner.on("fail", (test, err) => {
        if (
          err.message.includes(
            "AssertionError@https://cdn.jsdelivr.net/npm/chai/chai.js"
          )
        ) {
          // Handle or suppress specific assertion error
        } else {
          // Default error handling
          console.error("Test Failed:", err.message);
        }
      });
    }
  }

  return CustomReporter;
}
