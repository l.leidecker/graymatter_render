import React, { useState, useEffect } from "react";
import { MDBContainer, MDBBtn, MDBSpinner } from "mdb-react-ui-kit";
import { useNavigate } from "react-router-dom";
import axiosInstance from "../services/axiosConfig";
import ArrayView1 from "../components/Array/ArrayView1";
import ArrayView2 from "../components/Array/ArrayView2";
import ArrayView3 from "../components/Array/ArrayView3";
import ArrayView4 from "../components/Array/ArrayView4";
import ArrayScore from "../components/Array/ArrayScore";
import "./Array.css";

function Array() {
  // State to store userdata
  const [userData, setUserData] = useState(null);
  // State for loading animation
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    Promise.all([
      new Promise((resolve) => setTimeout(resolve, 1000)), // 1-second delay
      axiosInstance.get("/api/userdata/me/"), // API request
    ])
      .then(([, response]) => {
        setUserData(response.data); // Update state with the fetched data
        console.log("Fetched User Data:", response.data); // For debugging
      })
      .catch((error) => {
        console.error("Error fetching userdata:", error);
      })
      .finally(() => setIsLoading(false)); // Stop loading animation
  }, []);

  // Aufgaben States weitergereicht via props
  // Aufgabe 1 ArrayView2
  // Korrekt: "B" (1 Punkt)
  const [selectedQuestion1, setSelectedQuestion1] = useState("");
  // Korrekt: "B" (1 Punkt)
  const [selectedQuestion2, setSelectedQuestion2] = useState("");
  // Korrekt: "C" (1 Punkt)
  const [selectedQuestion3, setSelectedQuestion3] = useState("");
  // Korrekt: "A" (1 Punkt)
  const [selectedQuestion4, setSelectedQuestion4] = useState("");

  // Aufgabe 2 ArrayView4
  // Input Felder Lösung
  // Korrekt: 3 (1 Punkt)
  const [factorsCount, setFactorsCount] = useState("");
  // Korrekt: 3 (1 Punkt)
  const [levelsCount, setLevelsCount] = useState("");
  // Korrekt: 27 (1 Punkt)
  const [combinationCount, setCombinationCount] = useState("");

  // Table Array und Dropdown Lösung
  // Korrekt: 9 (1 Punkt)
  const [arrayLength, setArrayLength] = useState(1);

  // Array containing Arrays
  // Folgende Arrays sind korrekt (1 Punkt für jeden richtigen; Reihenfolge egal) ​
  // 0: Array(3) [ "Apple", "Red", "Low" ]
  // ​
  // 1: Array(3) [ "Apple", "Green", "Medium" ]
  // ​
  // 2: Array(3) [ "Apple", "Blue", "High" ]
  // ​
  // 3: Array(3) [ "Samsung", "Red", "Medium" ]
  // ​
  // 4: Array(3) [ "Samsung", "Green", "High" ]
  // ​
  // 5: Array(3) [ "Samsung", "Blue", "Low" ]
  // ​
  // 6: Array(3) [ "Sony", "Red", "High" ]
  // ​
  // 7: Array(3) [ "Sony", "Green", "Low" ]
  // ​
  // 8: Array(3) [ "Sony", "Blue", "Medium" ]
  const [dropdownValues, setDropdownValues] = useState([]);

  const [scores, setScores] = useState({
    scoreExercise1: 0,
    scoreExercise2: 0,
    overallScore: 0,
  });

  // State for controlling the modal
  const [modalOpen, setModalOpen] = useState(false);

  // Antworten auswerten
  const handleFinish = () => {
    // Stop the timer
    setTimerActive(false);

    // Scores for individual exercises
    let scoreExercise1 = 0;
    let scoreExercise2 = 0;

    // Exercise 1 Scoring
    scoreExercise1 += selectedQuestion1 === "B" ? 1 : 0; // "B" is correct for Question 1
    scoreExercise1 += selectedQuestion2 === "B" ? 1 : 0; // "B" is correct for Question 2
    scoreExercise1 += selectedQuestion3 === "C" ? 1 : 0; // "C" is correct for Question 3
    scoreExercise1 += selectedQuestion4 === "A" ? 1 : 0; // "A" is correct for Question 4

    // Exercise 2 Scoring
    scoreExercise2 += factorsCount === 3 ? 1 : 0; // "3" is correct for factorsCount
    scoreExercise2 += levelsCount === 3 ? 1 : 0; // "3" is correct for levelsCount
    scoreExercise2 += combinationCount === 27 ? 1 : 0; // "27" is correct for combinationCount
    scoreExercise2 += arrayLength === 9 ? 1 : 0; // "9" is correct for arrayLength

    const correctAnswers = [
      ["Apple", "Red", "Low"],
      ["Apple", "Green", "Medium"],
      ["Apple", "Blue", "High"],
      ["Samsung", "Red", "Medium"],
      ["Samsung", "Green", "High"],
      ["Samsung", "Blue", "Low"],
      ["Sony", "Red", "High"],
      ["Sony", "Green", "Low"],
      ["Sony", "Blue", "Medium"],
    ];

    correctAnswers.forEach((answer) => {
      const isCorrect = dropdownValues.some(
        (userAnswer) => JSON.stringify(userAnswer) === JSON.stringify(answer)
      );
      if (isCorrect) scoreExercise2 += 1;
    });

    // Calculate the overall score
    let overallScore = scoreExercise1 + scoreExercise2;

    // Log the scores
    console.log(`Score for Exercise 1: ${scoreExercise1}/4`);
    console.log(
      `Score for Exercise 2: ${scoreExercise2}/${4 + correctAnswers.length}`
    );
    console.log(
      `Overall Score: ${overallScore}/${4 + 4 + correctAnswers.length}`
    );

    // Update scores state
    setScores({
      scoreExercise1,
      scoreExercise2,
      overallScore,
      finalTime: elapsedTime,
    });

    // Update progress_array if necessary
    if (
      (overallScore >= 15 && userData.progress_array < 3) ||
      (overallScore >= 12 && userData.progress_array < 2) ||
      (overallScore >= 6 && userData.progress_array < 1)
    ) {
      // Determine the new progress level
      const newProgress = overallScore >= 15 ? 3 : overallScore >= 12 ? 2 : 1;

      console.log("Sending new progress_array:", newProgress);

      axiosInstance
        .patch("/api/userdata/me/", {
          progress_array: newProgress, // Send only progress_array
        })
        .then((response) => {
          console.log("Updated progress_array:", response.data.progress_array);
          // Update local userData state if needed
          setUserData((prevUserData) => ({
            ...prevUserData,
            progress_array: response.data.progress_array,
          }));
        })
        .catch((error) => {
          console.error("Error updating progress_array:", error);
        });
    }

    // Open the modal
    setModalOpen(true);
  };

  // State variable to keep track of the current view
  const [view, setView] = useState(1);

  // Slide Animation
  const [animationClass, setAnimationClass] = useState("");
  const [previousView, setPreviousView] = useState(view);

  useEffect(() => {
    if (view > previousView) {
      setAnimationClass("slide-in-right");
    } else if (view < previousView) {
      setAnimationClass("slide-in-left");
    }
    setPreviousView(view);
  }, [view]);

  // Returned aktuellen View
  const renderView = () => {
    switch (view) {
      case 1:
        return <ArrayView1 className={animationClass} />;
      case 2:
        return (
          <ArrayView2
            className={animationClass}
            selectedQuestion1={selectedQuestion1}
            setSelectedQuestion1={setSelectedQuestion1}
            selectedQuestion2={selectedQuestion2}
            setSelectedQuestion2={setSelectedQuestion2}
            selectedQuestion3={selectedQuestion3}
            setSelectedQuestion3={setSelectedQuestion3}
            selectedQuestion4={selectedQuestion4}
            setSelectedQuestion4={setSelectedQuestion4}
          />
        );
      case 3:
        return <ArrayView3 className={animationClass} />;
      case 4:
        return (
          <ArrayView4
            className={animationClass}
            factorsCount={factorsCount}
            setFactorsCount={setFactorsCount}
            levelsCount={levelsCount}
            setLevelsCount={setLevelsCount}
            combinationCount={combinationCount}
            setCombinationCount={setCombinationCount}
            arrayLength={arrayLength}
            setArrayLength={setArrayLength}
            dropdownValues={dropdownValues}
            setDropdownValues={setDropdownValues}
          />
        );
      default:
        return null;
    }
  };

  // React Router navigate
  const navigate = useNavigate();

  // Handler for the 'Weiter' button
  const handleNext = () => {
    if (view < 4) {
      setView(view + 1);
    } else {
      if (!modalOpen) {
        handleFinish();
      }
    }
  };

  // Handler für 'Zurück' button
  const handlePrev = () => {
    if (view > 1) setView(view - 1);
  };

  // Handler for the 'Abbrechen' button
  const handleAbort = () => {
    const userConfirmed = window.confirm(
      "Wollen Sie wirklich die Lerneinheit abbrechen und zur Lerneinheiten Übersicht zurückkehren?"
    );
    if (userConfirmed) {
      navigate("/units");
    }
  };

  // Timer:
  // State variable to hold the elapsed time in seconds
  const [elapsedTime, setElapsedTime] = useState(0);
  const [timerActive, setTimerActive] = useState(true);

  useEffect(() => {
    let timer;
    if (timerActive) {
      // Start a timer that updates every second
      timer = setInterval(() => {
        setElapsedTime((prevTime) => prevTime + 1);
      }, 1000);
    }

    // Cleanup: clear the timer when the component unmounts or timer stops
    return () => clearInterval(timer);
  }, [timerActive]);

  // Convert elapsedTime to minutes and seconds for display
  const minutes = String(Math.floor(elapsedTime / 60)).padStart(2, "0");
  const seconds = String(elapsedTime % 60).padStart(2, "0");

  if (isLoading) {
    return (
      <MDBContainer className="text-center">
        <MDBSpinner role="status" style={{ marginTop: "300px" }}>
          <span className="visually-hidden">Loading...</span>
        </MDBSpinner>
      </MDBContainer>
    );
  }

  return (
    <MDBContainer>
      <div className="d-flex justify-content-between align-items-center">
        <h2>Orthogonal-Array-Test</h2>
        <div className="d-flex align-items-center">
          <span
            className="me-3"
            style={{ fontWeight: "bold" }}
          >{`${minutes}:${seconds}`}</span>
          <MDBBtn size="sm" color="danger" onClick={handleAbort}>
            Abbrechen
          </MDBBtn>
        </div>
      </div>

      {/* Buttons right under the heading */}
      <div className="d-flex justify-content-between my-3">
        {view !== 1 ? (
          <MDBBtn outline size="sm" color="primary" onClick={handlePrev}>
            Zurück
          </MDBBtn>
        ) : (
          <div style={{ width: "fit-content" }}></div> // Placeholder div
        )}
        <MDBBtn
          size="sm"
          color={view === 4 ? "success" : "primary"}
          onClick={handleNext}
        >
          {view === 4 ? "Beenden" : "Weiter"}
        </MDBBtn>
      </div>

      {/* Conditionally render the content based on the view */}
      {renderView()}
      <ArrayScore isOpen={modalOpen} scores={scores} finalTime={elapsedTime} />
    </MDBContainer>
  );
}

export default Array;
