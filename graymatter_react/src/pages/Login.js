import React, { useState, useEffect, useCallback } from "react";
import axiosInstance from "../services/axiosConfig";
import { useNavigate } from "react-router-dom";

import Particles from "react-particles";
import { loadSlim } from "tsparticles-slim";

import "./Login.css";

import grayMatterIcon from "../assets/images/grayMatter_icon.png";

import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsContent,
  MDBTabsPane,
  MDBInput,
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
} from "mdb-react-ui-kit";

import { Alert } from "react-bootstrap";

function Login(props) {
  const [formData, setFormData] = useState({
    username: "",
    password: "",
  });

  const [registrationData, setRegistrationData] = useState({
    username: "",
    password: "",
    re_password: "",
  });

  const navigate = useNavigate();

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleRegisterChange = (event) => {
    setRegistrationData({
      ...registrationData,
      [event.target.name]: event.target.value,
    });
  };

  // MDBootstrap Alert bei Registrierung und Login
  const [alert, setAlert] = useState({
    type: "", // "success" oder "warning"
    message: "", // Nachricht an den Nutzer
  });

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await axiosInstance.post("/auth/jwt/create/", {
        username: formData.username,
        password: formData.password,
      });

      if (response.data && response.data.access) {
        localStorage.setItem("jwtToken", response.data.access);
        navigate("/dashboard");
      } else {
        console.error("Unexpected server response:", response);
        setAlert({
          type: "warning",
          message: "Login failed. Please check your credentials.",
        });
      }
    } catch (error) {
      console.error("Login error", error);
      if (error.response && error.response.status === 401) {
        // Handle unauthorized error (wrong credentials)
        setAlert({
          type: "warning",
          message: "Login failed. Please check your credentials.",
        });
      } else {
        // Handle other errors
        setAlert({
          type: "warning",
          message: "An unexpected error occurred. Please try again.",
        });
      }
    }
  };

  const handleRegistration = async (event) => {
    event.preventDefault();

    if (registrationData.password !== registrationData.re_password) {
      setAlert({
        type: "warning",
        message: "Passwörter stimmen nicht überein.",
      });
      return;
    }

    try {
      const response = await axiosInstance.post("/auth/users/", {
        username: registrationData.username,
        password: registrationData.password,
      });

      setAlert({
        type: "success",
        message: "Registrierung erfolgreich!",
      });
    } catch (error) {
      console.error("Registration error", error);
      setAlert({
        type: "warning",
        message: "Registrierung fehlgeschlagen. Bitte versuchen Sie es erneut.",
      });
    }
  };

  // Pill Tabs
  const [activeTab, setActiveTab] = useState("login");

  const handleTabClick = (value) => {
    if (value === activeTab) {
      return;
    }

    setActiveTab(value);
  };

  // Background Particles
  const particlesInit = useCallback(async (engine) => {
    console.log("Particles engine initialized", engine);
    await loadSlim(engine);
  }, []);

  const particlesLoaded = useCallback(async (container) => {
    console.log("Particles loaded", container);
  }, []);

  return (
    <MDBContainer
      fluid
      className="d-flex align-items-center justify-content-center vh-100"
    >
      <Particles
        id="tsparticles"
        init={particlesInit}
        loaded={particlesLoaded}
        url="/particles.json"
      />
      <MDBRow>
        <MDBCol>
          <MDBCard style={{ maxWidth: "400px" }}>
            <MDBCardHeader className="text-center header-flex">
              <h1>
                <img
                  src={grayMatterIcon}
                  alt="App"
                  className="img-fluid header-icon"
                />
                grayMatter
              </h1>
            </MDBCardHeader>
            <MDBCardBody>
              <MDBTabs pills fill className="me-2">
                <MDBTabsItem className="me-3">
                  <MDBTabsLink
                    onClick={() => handleTabClick("login")}
                    active={activeTab === "login"}
                  >
                    Anmelden
                  </MDBTabsLink>
                </MDBTabsItem>
                <MDBTabsItem>
                  <MDBTabsLink
                    onClick={() => handleTabClick("register")}
                    active={activeTab === "register"}
                  >
                    Registrieren
                  </MDBTabsLink>
                </MDBTabsItem>
              </MDBTabs>

              <MDBTabsContent>
                <MDBTabsPane show={activeTab === "login"}>
                  <form onSubmit={handleSubmit}>
                    <MDBInput
                      label="Nutzername"
                      name="username"
                      className="my-3"
                      value={formData.username}
                      onChange={handleChange}
                      required
                    />
                    <MDBInput
                      label="Passwort"
                      type="password"
                      name="password"
                      className="mb-3"
                      value={formData.password}
                      onChange={handleChange}
                      required
                    />
                    <MDBBtn type="submit" className="w-100">
                      Anmelden
                    </MDBBtn>
                    {alert.message && (
                      <Alert variant={alert.type} className="mt-3">
                        {alert.message}
                      </Alert>
                    )}
                  </form>
                </MDBTabsPane>

                <MDBTabsPane show={activeTab === "register"}>
                  <form onSubmit={handleRegistration}>
                    <MDBInput
                      label="Nutzername"
                      name="username"
                      className="my-3"
                      value={registrationData.username}
                      onChange={handleRegisterChange}
                      required
                    />
                    <MDBInput
                      label="Passwort"
                      type="password"
                      name="password"
                      className="mb-3"
                      value={registrationData.password}
                      onChange={handleRegisterChange}
                      required
                    />
                    <MDBInput
                      label="Passwort wiederholen"
                      type="password"
                      name="re_password"
                      className="mb-3"
                      value={registrationData.re_password}
                      onChange={handleRegisterChange}
                      required
                    />
                    <MDBBtn type="submit" className="w-100">
                      Registrieren
                    </MDBBtn>
                    {alert.message && (
                      <Alert variant={alert.type} className="mt-3">
                        {alert.message}
                      </Alert>
                    )}
                  </form>
                </MDBTabsPane>
              </MDBTabsContent>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
}

export default Login;
