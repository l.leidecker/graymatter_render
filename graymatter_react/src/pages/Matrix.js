import React, { useState, useEffect } from "react";
import { MDBContainer, MDBBtn } from "mdb-react-ui-kit";
import { useNavigate } from "react-router-dom";
import MatrixView1 from "../components/Matrix/MatrixView1";
import MatrixView2 from "../components/Matrix/MatrixView2";
import MatrixView3 from "../components/Matrix/MatrixView3";
import "./Matrix.css";

function Matrix() {
  // State variable to keep track of the current view
  const [view, setView] = useState(1);

  // Slide Animation
  const [animationClass, setAnimationClass] = useState("");
  const [previousView, setPreviousView] = useState(view);

  useEffect(() => {
    if (view > previousView) {
      setAnimationClass("slide-in-right");
    } else if (view < previousView) {
      setAnimationClass("slide-in-left");
    }
    setPreviousView(view);
  }, [view]);

  // Returned aktuellen View
  const renderView = () => {
    switch (view) {
      case 1:
        return <MatrixView1 className={animationClass} />;
      case 2:
        return <MatrixView2 className={animationClass} />;
      case 3:
        return <MatrixView3 className={animationClass} />;
      default:
        return null;
    }
  };

  // React Router navigate
  const navigate = useNavigate();

  // Handler for the 'Weiter' button
  const handleNext = () => {
    if (view < 3) setView(view + 1);
  };

  // Handler for the 'Zurück' button
  const handlePrev = () => {
    if (view > 1) setView(view - 1);
  };

  // Handler for the 'Abbrechen' button
  const handleAbort = () => {
    const userConfirmed = window.confirm(
      "Wollen Sie wirklich die Lerneinheit abbrechen und zur Lerneinheiten Übersicht zurückkehren?"
    );
    if (userConfirmed) {
      navigate("/units");
    }
  };

  // Timer:
  // State variable to hold the elapsed time in seconds
  const [elapsedTime, setElapsedTime] = useState(0);

  useEffect(() => {
    // Start a timer that updates every second
    const timer = setInterval(() => {
      setElapsedTime((prevTime) => prevTime + 1);
    }, 1000);

    // Cleanup: clear the timer when the component unmounts
    return () => clearInterval(timer);
  }, []);

  // Convert elapsedTime to minutes and seconds for display
  const minutes = String(Math.floor(elapsedTime / 60)).padStart(2, "0");
  const seconds = String(elapsedTime % 60).padStart(2, "0");

  return (
    <MDBContainer>
      <div className="d-flex justify-content-between align-items-center">
        <h2>Matrix-Test</h2>
        <div className="d-flex align-items-center">
          <span
            className="me-3"
            style={{ fontWeight: "bold" }}
          >{`${minutes}:${seconds}`}</span>
          <MDBBtn size="sm" color="danger" onClick={handleAbort}>
            Abbrechen
          </MDBBtn>
        </div>
      </div>

      {/* Buttons right under the heading */}
      <div className="d-flex justify-content-between my-3">
        {view !== 1 ? (
          <MDBBtn outline size="sm" color="primary" onClick={handlePrev}>
            Zurück
          </MDBBtn>
        ) : (
          <div style={{ width: "fit-content" }}></div> // Placeholder div
        )}
        <MDBBtn
          size="sm"
          color={view === 3 ? "success" : "primary"}
          onClick={handleNext}
        >
          {view === 3 ? "Beenden" : "Weiter"}
        </MDBBtn>
      </div>

      {/* Conditionally render the content based on the view */}
      {renderView()}
    </MDBContainer>
  );
}

export default Matrix;
