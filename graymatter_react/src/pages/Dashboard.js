import React, { useState, useEffect } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBSpinner } from "mdb-react-ui-kit"; // Import MDBSpinner

import axiosInstance from "../services/axiosConfig";

import ProfileCard from "../components/Dashboard/ProfileCard";
import Leaderboard from "../components/Dashboard/Leaderboard";
import TheoryRecommendedCard from "../components/Dashboard/TheoryRecommendedCard";
import MatrixRecommendedCard from "../components/Dashboard/MatrixRecommendedCard";
import ArrayRecommendedCard from "../components/Dashboard/ArrayRecommendedCard";
import PatternRecommendedCard from "../components/Dashboard/PatternRecommendedCard";
import RegressionRecommendedCard from "../components/Dashboard/RegressionRecommendedCard";

function Dashboard() {
  const [personalData, setPersonalData] = useState(null);
  const [allUserData, setAllUserData] = useState(null);
  const [isLoading, setIsLoading] = useState(true); // State for loading animation

  useEffect(() => {
    Promise.all([
      new Promise((resolve) => setTimeout(resolve, 1000)), // 1-second delay
      axiosInstance
        .get("/api/userdata/me/")
        .then((response) => setPersonalData(response.data)), // Fetch personal data
      axiosInstance
        .get("/api/userdata/")
        .then((response) => setAllUserData(response.data)), // Fetch all user data
    ])
      .catch((error) => {
        console.error("Error fetching data:", error);
      })
      .finally(() => setIsLoading(false)); // Stop loading animation once both requests are complete
  }, []);

  // Function to determine the recommended card
  const getRecommendedCard = (data) => {
    if (!data) return null;

    const {
      progress_theory,
      progress_matrix,
      progress_array,
      progress_pattern,
      progress_regression,
    } = data;

    if (progress_theory === 0)
      return <TheoryRecommendedCard personalData={personalData} />;
    if (progress_matrix === 0)
      return <MatrixRecommendedCard personalData={personalData} />;
    if (progress_array === 0)
      return <ArrayRecommendedCard personalData={personalData} />;
    if (progress_pattern === 0)
      return <PatternRecommendedCard personalData={personalData} />;
    if (progress_regression === 0)
      return <RegressionRecommendedCard personalData={personalData} />;

    // Find the unit with the lowest progress
    const progresses = [
      progress_theory,
      progress_matrix,
      progress_array,
      progress_pattern,
      progress_regression,
    ];
    const minProgress = Math.min(...progresses);

    if (progresses.every((p) => p === 3))
      return <RegressionRecommendedCard personalData={personalData} />;
    if (minProgress === progress_theory)
      return <TheoryRecommendedCard personalData={personalData} />;
    if (minProgress === progress_matrix)
      return <MatrixRecommendedCard personalData={personalData} />;
    if (minProgress === progress_array)
      return <ArrayRecommendedCard personalData={personalData} />;
    if (minProgress === progress_pattern)
      return <PatternRecommendedCard personalData={personalData} />;

    return <RegressionRecommendedCard personalData={personalData} />;
  };

  if (isLoading) {
    return (
      <MDBContainer className="text-center">
        <MDBSpinner role="status" style={{ marginTop: "300px" }}>
          <span className="visually-hidden">Loading...</span>
        </MDBSpinner>
      </MDBContainer>
    );
  }

  return (
    <MDBContainer>
      <h2 className="mb-3">Dashboard</h2>
      <MDBRow>
        <MDBCol lg="8">
          <ProfileCard personalData={personalData} allUserData={allUserData} />
          {getRecommendedCard(personalData)}
        </MDBCol>
        <MDBCol lg="4">
          <Leaderboard allUserData={allUserData} personalData={personalData} />
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
}

export default Dashboard;
