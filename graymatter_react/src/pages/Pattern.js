import React, { useState, useEffect } from "react";
import { MDBContainer, MDBBtn, MDBSpinner } from "mdb-react-ui-kit";
import { useNavigate } from "react-router-dom";
import axiosInstance from "../services/axiosConfig";
import PatternView1 from "../components/Pattern/PatternView1";
import PatternView2 from "../components/Pattern/PatternView2";
import PatternView3 from "../components/Pattern/PatternView3";
import PatternView4 from "../components/Pattern/PatternView4";
import PatternScore from "../components/Pattern/PatternScore";
import "./Pattern.css";

function Pattern() {
  // State to store userdata
  const [userData, setUserData] = useState(null);
  // State for loading animation
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    Promise.all([
      new Promise((resolve) => setTimeout(resolve, 1000)), // 1-second delay
      axiosInstance.get("/api/userdata/me/"), // API request
    ])
      .then(([, response]) => {
        setUserData(response.data); // Update state with the fetched data
        console.log("Fetched User Data:", response.data); // For debugging
      })
      .catch((error) => {
        console.error("Error fetching userdata:", error);
      })
      .finally(() => setIsLoading(false)); // Stop loading animation
  }, []);

  // Antworten der Fragen in den verschiedenen Views (als props weitergereicht)
  // Jede korrekte Antwort = 1 Punkt (insgesamt 6 Punkte)
  // 6 Punkte = "Gold" ; 5 Punkte = "Silber" ; 3 oder 4 Punkte = "Bronze" ; Unter 3 = "Durchgefallen"
  // PatternView2
  // "A" korrekt
  const [selectedQuestion1, setSelectedQuestion1] = useState("");
  // "B" korrekt
  const [selectedQuestion2, setSelectedQuestion2] = useState("");
  // "C" korrekt
  const [selectedQuestion3, setSelectedQuestion3] = useState("");

  // PatternView3 -> PatternIdentification
  // "SQL Injection" korrekt
  const [selectedOption, setSelectedOption] = useState("");
  const [charInput, setCharInput] = useState("");
  // true korrekt
  const [isCorrect, setIsCorrect] = useState(null);

  // PatternView4 -> PatternDebug
  // true correct
  const [debugCorrect, setDebugCorrect] = useState(false);

  //   const initialCode = `
  //   function sanitize(input) {
  //     // Code here
  //     return input
  //     .replace(/['";#&]/g, "") // Remove single quotes, double quotes, semicolons, hashes, and ampersands
  //     .replace(/--/g, "") // Remove double hyphens
  //     .replace(/\//g, "") // Remove forward slashes
  //     .replace(/\*/g, "") // Remove asterisks
  //     .replace(/\|/g, ""); // Remove pipes
  //   }
  // `;

  // State for controlling the modal
  const [modalOpen, setModalOpen] = useState(false);
  const [scores, setScores] = useState({
    scoreExercise1: 0,
    scoreExercise2: 0,
    scoreExercise3: 0,
    overallScore: 0,
  });

  // Antworten auswerten
  const handleFinish = () => {
    // Stop the timer
    setTimerActive(false);

    // Scores for individual exercises
    let scoreExercise1 = 0;
    let scoreExercise2 = 0;
    let scoreExercise3 = 0;

    // Exercise 1 Scoring
    scoreExercise1 += selectedQuestion1 === "A" ? 1 : 0; // "A" is correct for Question 1
    scoreExercise1 += selectedQuestion2 === "B" ? 1 : 0; // "B" is correct for Question 2
    scoreExercise1 += selectedQuestion3 === "C" ? 1 : 0; // "C" is correct for Question 3

    // Exercise 2 Scoring
    scoreExercise2 += selectedOption === "SQL Injection" ? 1 : 0; // "SQL Injection" is correct for Pattern Identification
    scoreExercise2 += isCorrect === true ? 1 : 0; // isCorrect being true is correct

    // Exercise 3 Scoring
    scoreExercise3 += debugCorrect === true ? 1 : 0; // debugCorrect being true is correct

    // Calculate the overall score
    let overallScore = scoreExercise1 + scoreExercise2 + scoreExercise3;

    // Log the scores
    console.log(`Score for Exercise 1: ${scoreExercise1}/3`);
    console.log(`Score for Exercise 2: ${scoreExercise2}/2`);
    console.log(`Score for Exercise 3: ${scoreExercise3}/1`);
    console.log(`Overall Score: ${overallScore}/6`);

    // Update scores state
    setScores({
      scoreExercise1,
      scoreExercise2,
      scoreExercise3,
      overallScore,
      finalTime: elapsedTime,
    });

    // Update progress_pattern if necessary
    if (
      (overallScore === 6 && userData.progress_pattern < 3) ||
      (overallScore === 5 && userData.progress_pattern < 2) ||
      ((overallScore === 4 || overallScore === 3) &&
        userData.progress_pattern < 1)
    ) {
      // Determine the new progress level
      const newProgress = overallScore === 6 ? 3 : overallScore === 5 ? 2 : 1;

      console.log("Sending new progress_pattern:", newProgress);

      axiosInstance
        .patch("/api/userdata/me/", {
          progress_pattern: newProgress, // Send progress_pattern
        })
        .then((response) => {
          console.log(
            "Updated progress_pattern:",
            response.data.progress_pattern
          );
          // Update local userData state if needed
          setUserData((prevUserData) => ({
            ...prevUserData,
            progress_pattern: response.data.progress_pattern,
          }));
        })
        .catch((error) => {
          console.error("Error updating progress_pattern:", error);
        });
    }

    // Open the modal
    setModalOpen(true);
  };

  // State variable to keep track of the current view
  const [view, setView] = useState(1);

  // Slide Animation
  const [animationClass, setAnimationClass] = useState("");
  const [previousView, setPreviousView] = useState(view);

  useEffect(() => {
    if (view > previousView) {
      setAnimationClass("slide-in-right");
    } else if (view < previousView) {
      setAnimationClass("slide-in-left");
    }
    setPreviousView(view);
  }, [view]);

  // Returned aktuellen View
  const renderView = () => {
    switch (view) {
      case 1:
        return <PatternView1 className={animationClass} />;
      case 2:
        return (
          <PatternView2
            className={animationClass}
            selectedQuestion1={selectedQuestion1}
            setSelectedQuestion1={setSelectedQuestion1}
            selectedQuestion2={selectedQuestion2}
            setSelectedQuestion2={setSelectedQuestion2}
            selectedQuestion3={selectedQuestion3}
            setSelectedQuestion3={setSelectedQuestion3}
          />
        );
      case 3:
        return (
          <PatternView3
            className={animationClass}
            selectedOption={selectedOption}
            setSelectedOption={setSelectedOption}
            charInput={charInput}
            setCharInput={setCharInput}
            isCorrect={isCorrect}
            setIsCorrect={setIsCorrect}
          />
        );
      case 4:
        return (
          <PatternView4
            className={animationClass}
            debugCorrect={debugCorrect}
            setDebugCorrect={setDebugCorrect}
          />
        );
      default:
        return null;
    }
  };

  // React Router navigate
  const navigate = useNavigate();

  // Handler for the 'Weiter' button
  const handleNext = () => {
    if (view < 4) {
      setView(view + 1);
    } else {
      if (!modalOpen) {
        handleFinish();
      }
    }
  };

  // Handler for the 'Zurück' button
  const handlePrev = () => {
    if (view > 1) setView(view - 1);
  };

  // Handler for the 'Abbrechen' button
  const handleAbort = () => {
    const userConfirmed = window.confirm(
      "Wollen Sie wirklich die Lerneinheit abbrechen und zur Lerneinheiten Übersicht zurückkehren?"
    );
    if (userConfirmed) {
      navigate("/units");
    }
  };

  // Timer:
  // State variable to hold the elapsed time in seconds
  const [elapsedTime, setElapsedTime] = useState(0);
  const [timerActive, setTimerActive] = useState(true);

  useEffect(() => {
    let timer;
    if (timerActive) {
      // Start a timer that updates every second
      timer = setInterval(() => {
        setElapsedTime((prevTime) => prevTime + 1);
      }, 1000);
    }

    // Cleanup: clear the timer when the component unmounts or timer stops
    return () => clearInterval(timer);
  }, [timerActive]);

  // Convert elapsedTime to minutes and seconds for display
  const minutes = String(Math.floor(elapsedTime / 60)).padStart(2, "0");
  const seconds = String(elapsedTime % 60).padStart(2, "0");

  if (isLoading) {
    return (
      <MDBContainer className="text-center">
        <MDBSpinner role="status" style={{ marginTop: "300px" }}>
          <span className="visually-hidden">Loading...</span>
        </MDBSpinner>
      </MDBContainer>
    );
  }

  return (
    <MDBContainer>
      <div className="d-flex justify-content-between align-items-center">
        <h2>Pattern-Test</h2>
        <div className="d-flex align-items-center">
          <span
            className="me-3"
            style={{ fontWeight: "bold" }}
          >{`${minutes}:${seconds}`}</span>
          <MDBBtn size="sm" color="danger" onClick={handleAbort}>
            Abbrechen
          </MDBBtn>
        </div>
      </div>

      {/* Buttons right under the heading */}
      <div className="d-flex justify-content-between my-2">
        {view !== 1 ? (
          <MDBBtn outline size="sm" color="primary" onClick={handlePrev}>
            Zurück
          </MDBBtn>
        ) : (
          <div style={{ width: "fit-content" }}></div> // Placeholder div
        )}
        <MDBBtn
          size="sm"
          color={view === 4 ? "success" : "primary"}
          onClick={handleNext}
        >
          {view === 4 ? "Beenden" : "Weiter"}
        </MDBBtn>
      </div>

      {/* Conditionally render the content based on the view */}
      {renderView()}
      <PatternScore
        isOpen={modalOpen}
        scores={scores}
        finalTime={elapsedTime}
        userData={userData}
      />
    </MDBContainer>
  );
}

export default Pattern;
