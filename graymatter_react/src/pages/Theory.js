import React, { useState, useEffect } from "react";
import { MDBContainer, MDBBtn, MDBSpinner } from "mdb-react-ui-kit";
import { useNavigate } from "react-router-dom";
import axiosInstance from "../services/axiosConfig";
import TheoryView1 from "../components/Theory/TheoryView1";
import TheoryView2 from "../components/Theory/TheoryView2";
import TheoryView3 from "../components/Theory/TheoryView3";
import TheoryView4 from "../components/Theory/TheoryView4";
import TheoryScore from "../components/Theory/TheoryScore";
import "./Theory.css";

function Theory() {
  // State to store userdata
  const [userData, setUserData] = useState(null);
  // State for loading animation
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    Promise.all([
      new Promise((resolve) => setTimeout(resolve, 1000)), // 1-second delay
      axiosInstance.get("/api/userdata/me/"), // API request
    ])
      .then(([, response]) => {
        setUserData(response.data); // Update state with the fetched data
        console.log("Fetched User Data:", response.data); // For debugging
      })
      .catch((error) => {
        console.error("Error fetching userdata:", error);
      })
      .finally(() => setIsLoading(false)); // Stop loading animation
  }, []);

  const [scores, setScores] = useState({
    scoreExercise1: 0,
    scoreExercise2: 0,
    overallScore: 0,
  });

  // State for controlling the modal
  const [modalOpen, setModalOpen] = useState(false);

  // States Aufgabe 1 -> TheoryView2 (Punktzahl ingsesamt: 4)
  // Korrekt: "C" (1 Punkt)
  const [selectedQuestion1, setSelectedQuestion1] = useState("");
  // Korrekt: "C" (1 Punkt)
  const [selectedQuestion2, setSelectedQuestion2] = useState("");
  // Korrekt: "B" (1 Punkt)
  const [selectedQuestion3, setSelectedQuestion3] = useState("");
  // Korrekt: "C" (1 Punkt)
  const [selectedQuestion4, setSelectedQuestion4] = useState("");

  // States Aufgabe 2 -> TheoryView4 (Punktzahl ingsesamt: 3)
  // Korrekt: "B" (1 Punkt)
  const [selectedQuestion5, setSelectedQuestion5] = useState("");
  // Korrekt: "C" (1 Punkt)
  const [selectedQuestion6, setSelectedQuestion6] = useState("");
  // Korrekt: "A" (1 Punkt)
  const [selectedQuestion7, setSelectedQuestion7] = useState("");

  const handleQuestionChange = (questionNumber, value) => {
    switch (questionNumber) {
      case 1:
        setSelectedQuestion1(value);
        break;
      case 2:
        setSelectedQuestion2(value);
        break;
      case 3:
        setSelectedQuestion3(value);
        break;
      case 4:
        setSelectedQuestion4(value);
        break;
      case 5:
        setSelectedQuestion5(value);
        break;
      case 6:
        setSelectedQuestion6(value);
        break;
      case 7:
        setSelectedQuestion7(value);
        break;
      default:
    }
  };

  // Antworten auswerten
  const handleFinish = () => {
    // Stop the timer
    setTimerActive(false);

    // Scores for individual exercises
    let scoreExercise1 = 0;
    let scoreExercise2 = 0;

    // Exercise 1 Scoring
    scoreExercise1 += selectedQuestion1 === "C" ? 1 : 0; // "C" is correct for Question 1
    scoreExercise1 += selectedQuestion2 === "C" ? 1 : 0; // "C" is correct for Question 2
    scoreExercise1 += selectedQuestion3 === "B" ? 1 : 0; // "B" is correct for Question 3
    scoreExercise1 += selectedQuestion4 === "C" ? 1 : 0; // "C" is correct for Question 4

    // Exercise 2 Scoring
    scoreExercise2 += selectedQuestion5 === "B" ? 1 : 0; // "B" is correct for Question 1
    scoreExercise2 += selectedQuestion6 === "C" ? 1 : 0; // "C" is correct for Question 2
    scoreExercise2 += selectedQuestion7 === "A" ? 1 : 0; // "A" is correct for Question 3

    // Calculate the overall score
    let overallScore = scoreExercise1 + scoreExercise2;

    // Log the scores
    console.log(`Score for Exercise 1: ${scoreExercise1}/4`);
    console.log(`Score for Exercise 2: ${scoreExercise2}/3`);
    console.log(`Overall Score: ${overallScore}/7`);

    // Update scores state
    setScores({
      scoreExercise1,
      scoreExercise2,
      overallScore,
      finalTime: elapsedTime,
    });

    // Update progress_theory if necessary
    if (
      (overallScore === 7 && userData.progress_theory < 3) ||
      ((overallScore === 6 || overallScore === 5) &&
        userData.progress_theory < 2) ||
      ((overallScore === 4 || overallScore === 3) &&
        userData.progress_theory < 1)
    ) {
      // Determine the new progress level
      const newProgress =
        overallScore === 7
          ? 3
          : overallScore === 6 || overallScore === 5
          ? 2
          : 1;

      console.log("Sending new progress_theory:", newProgress);

      axiosInstance
        .patch("/api/userdata/me/", {
          progress_theory: newProgress, // Send only progress_theory
        })
        .then((response) => {
          console.log(
            "Updated progress_theory:",
            response.data.progress_theory
          );
          // Update local userData state if needed
          setUserData((prevUserData) => ({
            ...prevUserData,
            progress_theory: response.data.progress_theory,
          }));
        })
        .catch((error) => {
          console.error("Error updating progress_theory:", error);
        });
    }

    // Open the modal
    setModalOpen(true);
  };

  // State variable to keep track of the current view
  const [view, setView] = useState(1);

  // Slide Animation
  const [animationClass, setAnimationClass] = useState("");
  const [previousView, setPreviousView] = useState(view);

  useEffect(() => {
    if (view > previousView) {
      setAnimationClass("slide-in-right");
    } else if (view < previousView) {
      setAnimationClass("slide-in-left");
    }
    setPreviousView(view);
  }, [view]);

  // Returned aktuellen View
  const renderView = () => {
    switch (view) {
      case 1:
        return <TheoryView1 className={animationClass} />;
      case 2:
        return (
          <TheoryView2
            className={animationClass}
            selectedQuestion1={selectedQuestion1}
            setSelectedQuestion1={setSelectedQuestion1}
            selectedQuestion2={selectedQuestion2}
            setSelectedQuestion2={setSelectedQuestion2}
            selectedQuestion3={selectedQuestion3}
            setSelectedQuestion3={setSelectedQuestion3}
            selectedQuestion4={selectedQuestion4}
            setSelectedQuestion4={setSelectedQuestion4}
            handleQuestionChange={handleQuestionChange}
          />
        );
      case 3:
        return <TheoryView3 className={animationClass} />;
      case 4:
        return (
          <TheoryView4
            className={animationClass}
            selectedQuestion5={selectedQuestion5}
            setSelectedQuestion5={setSelectedQuestion5}
            selectedQuestion6={selectedQuestion6}
            setSelectedQuestion6={setSelectedQuestion6}
            selectedQuestion7={selectedQuestion7}
            setSelectedQuestion7={setSelectedQuestion7}
            handleQuestionChange={handleQuestionChange}
          />
        );
      default:
        return null;
    }
  };

  // React Router navigate
  const navigate = useNavigate();

  // Handler for the 'Weiter' button
  const handleNext = () => {
    if (view < 4) {
      setView(view + 1);
    } else {
      if (!modalOpen) {
        handleFinish();
      }
    }
  };

  // Handler for the 'Zurück' button
  const handlePrev = () => {
    if (view > 1) setView(view - 1);
  };

  // Handler for the 'Abbrechen' button
  const handleAbort = () => {
    const userConfirmed = window.confirm(
      "Wollen Sie wirklich die Lerneinheit abbrechen und zur Lerneinheiten Übersicht zurückkehren?"
    );
    if (userConfirmed) {
      navigate("/units");
    }
  };

  // Timer:
  // State variable to hold the elapsed time in seconds
  const [elapsedTime, setElapsedTime] = useState(0);
  const [timerActive, setTimerActive] = useState(true);

  useEffect(() => {
    let timer;
    if (timerActive) {
      // Start a timer that updates every second
      timer = setInterval(() => {
        setElapsedTime((prevTime) => prevTime + 1);
      }, 1000);
    }

    // Cleanup: clear the timer when the component unmounts or timer stops
    return () => clearInterval(timer);
  }, [timerActive]);

  // Convert elapsedTime to minutes and seconds for display
  const minutes = String(Math.floor(elapsedTime / 60)).padStart(2, "0");
  const seconds = String(elapsedTime % 60).padStart(2, "0");

  if (isLoading) {
    return (
      <MDBContainer className="text-center">
        <MDBSpinner role="status" style={{ marginTop: "300px" }}>
          <span className="visually-hidden">Loading...</span>
        </MDBSpinner>
      </MDBContainer>
    );
  }

  return (
    <MDBContainer>
      <div className="d-flex justify-content-between align-items-center">
        <h2>Theoretische Grundlagen</h2>
        <div className="d-flex align-items-center">
          <span
            className="me-3"
            style={{ fontWeight: "bold" }}
          >{`${minutes}:${seconds}`}</span>
          <MDBBtn size="sm" color="danger" onClick={handleAbort}>
            Abbrechen
          </MDBBtn>
        </div>
      </div>

      {/* Buttons right under the heading */}
      <div className="d-flex justify-content-between my-3">
        {view !== 1 ? (
          <MDBBtn outline size="sm" color="primary" onClick={handlePrev}>
            Zurück
          </MDBBtn>
        ) : (
          <div style={{ width: "fit-content" }}></div> // Placeholder div
        )}
        <MDBBtn
          size="sm"
          color={view === 4 ? "success" : "primary"}
          onClick={handleNext}
        >
          {view === 4 ? "Beenden" : "Weiter"}
        </MDBBtn>
      </div>

      {/* Conditionally render the content based on the view */}
      {renderView()}
      <TheoryScore isOpen={modalOpen} scores={scores} finalTime={elapsedTime} />
    </MDBContainer>
  );
}

export default Theory;
