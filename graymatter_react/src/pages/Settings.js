import React, { useState, useEffect } from "react";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBTooltip,
  MDBSpinner,
} from "mdb-react-ui-kit";
import axiosInstance from "../services/axiosConfig";

// Importing profile images
import absolvent from "../assets/images/profile/absolvent.png";
import alien from "../assets/images/profile/alien.png";
import amira from "../assets/images/profile/amira.png";
import ammelie from "../assets/images/profile/ammelie.png";
import anna from "../assets/images/profile/anna.png";
import babsi from "../assets/images/profile/babsi.png";
import burak from "../assets/images/profile/burak.png";
import defaultAvatar from "../assets/images/profile/default.png";
import dennis from "../assets/images/profile/dennis.png";
import fabian from "../assets/images/profile/fabian.png";
import herbert from "../assets/images/profile/herbert.png";
import hero from "../assets/images/profile/hero.png";
import lena from "../assets/images/profile/lena.png";
import lisa from "../assets/images/profile/lisa.png";
import maike from "../assets/images/profile/maike.png";
import mark from "../assets/images/profile/mark.png";
import otto from "../assets/images/profile/otto.png";
import roboter from "../assets/images/profile/roboter.png";
import transzendenz from "../assets/images/profile/transzendenz.png";
import vincent from "../assets/images/profile/vincent.png";

import "./Settings.css";

function Settings() {
  // State for loading animation
  const [isLoading, setIsLoading] = useState(true);

  const [userData, setUserData] = useState({});
  const [selectedPic, setSelectedPic] = useState("");

  useEffect(() => {
    Promise.all([
      new Promise((resolve) => setTimeout(resolve, 1000)), // Ensure loading for at least 1 second
      axiosInstance.get("/api/userdata/me/").then((response) => {
        setUserData(response.data);
        setSelectedPic(response.data.profile_pic);
      }),
    ])
      .catch((error) => {
        console.error("Error fetching user data:", error);
      })
      .finally(() => setIsLoading(false)); // Stop the loading animation
  }, []);

  // Reset progress handler
  const handleResetProgress = () => {
    if (
      window.confirm(
        "Wollen Sie ihren Fortschritt wirklich zurücksetzen? Dieser Vorgang ist irreversibel."
      )
    ) {
      axiosInstance
        .patch("/api/userdata/me/", {
          progress_theory: 0,
          progress_matrix: 0,
          progress_array: 0,
          progress_pattern: 0,
          progress_regression: 0,
          profile_pic: "default",
        })
        .then((response) => {
          // Handle successful response
          console.log("Progress reset successful:", response.data);
          window.location.reload(); // Reload the page to reflect changes
        })
        .catch((error) => {
          // Handle error
          console.error("Error resetting progress:", error);
        });
    }
  };

  // Function to handle profile picture click
  const handleProfilePicClick = (picName, isLocked) => {
    if (!isLocked) {
      setSelectedPic(picName);
      // Call the API to update the profile picture
      axiosInstance
        .patch("/api/userdata/me/", { profile_pic: picName })
        .then((response) => {
          // Handle successful response
          console.log("Profile picture updated:", response.data);
        })
        .catch((error) => {
          // Handle error
          console.error("Error updating profile picture:", error);
        });
    }
  };

  // Check if a specific profile picture should be locked based on user progress
  const isLocked = (picName) => {
    switch (picName) {
      case "absolvent":
        return userData.progress_theory < 3;
      case "roboter":
        return userData.progress_matrix < 3;
      case "hero":
        return userData.progress_array < 3;
      case "alien":
        return userData.progress_pattern < 3;
      case "transzendenz":
        return userData.progress_regression < 3;
      default:
        return false; // Other pictures are not locked
    }
  };

  // Function to get the tooltip message for a locked picture
  const getTooltipMessage = (picName) => {
    const tooltips = {
      absolvent: "Zum freischalten 'Theoretische Grundlagen' Gold erreichen.",
      roboter: "Zum freischalten 'Matrix-Test' Gold erreichen.",
      hero: "Zum freischalten 'Orthogonale-Array-Test' Gold erreichen.",
      alien: "Zum freischalten 'Pattern-Test' Gold erreichen.",
      transzendenz: "Zum freischalten 'Regressionstest' Gold erreichen.",
    };
    return tooltips[picName] || ""; // Return an empty string if the picture is not in the list
  };

  // Object to map picture names to image imports
  const profileImages = {
    absolvent,
    alien,
    amira,
    ammelie,
    anna,
    babsi,
    burak,
    default: defaultAvatar,
    dennis,
    fabian,
    herbert,
    hero,
    lena,
    lisa,
    maike,
    mark,
    otto,
    roboter,
    transzendenz,
    vincent,
  };

  // Manually ordered array of profile picture names
  const profilePics = [
    "amira",
    "ammelie",
    "anna",
    "babsi",
    "burak",
    "default",
    "dennis",
    "fabian",
    "herbert",
    "lena",
    "lisa",
    "maike",
    "mark",
    "otto",
    "vincent",
    "absolvent",
    "roboter",
    "hero",
    "alien",
    "transzendenz",
  ];

  // Function to create a grid of profile pictures
  const createPictureGrid = () => {
    // Split the profilePics array into groups of 5
    const rows = [];
    for (let i = 0; i < profilePics.length; i += 5) {
      rows.push(profilePics.slice(i, i + 5));
    }

    return rows.map((row, rowIndex) => (
      <MDBRow
        key={rowIndex}
        className="mb-3"
        style={{ justifyContent: "center" }}
      >
        {row.map((picName, index) => {
          const locked = isLocked(picName);
          const tooltipMessage = locked ? getTooltipMessage(picName) : "";
          return (
            <div
              key={picName}
              className="d-flex justify-content-center mb-3"
              style={{ width: "20%" }} // Each image takes up 20% of the row width
            >
              <MDBTooltip tag="div" title={tooltipMessage}>
                <MDBBtn
                  className={`profile-pic-btn ${
                    selectedPic === picName ? "selected" : ""
                  } ${locked ? "locked" : ""}`}
                  onClick={() => handleProfilePicClick(picName, locked)}
                  style={{ padding: 0, position: "relative" }}
                  disabled={locked}
                >
                  <img
                    src={profileImages[picName]}
                    alt={picName}
                    className={`img-fluid profile-pic-img ${
                      locked ? "grayscale" : ""
                    }`}
                  />
                  {selectedPic === picName && !locked && (
                    <div className="selected-border"></div>
                  )}
                </MDBBtn>
              </MDBTooltip>
            </div>
          );
        })}
      </MDBRow>
    ));
  };

  return isLoading ? (
    <MDBContainer className="text-center">
      <MDBSpinner role="status" style={{ marginTop: "300px" }}>
        <span className="visually-hidden">Loading...</span>
      </MDBSpinner>
    </MDBContainer>
  ) : (
    <MDBContainer className="settings-container mb-5">
      <h2>Einstellungen</h2>
      <p>
        Angemeldet als <i>{userData.user_name}</i>
      </p>
      <h4>Profilbild auswählen</h4>
      <p>
        Aktuell ausgewähltes Bild: <strong>{selectedPic}</strong>
      </p>
      {/* Padding columns for larger screens */}
      <MDBRow>
        <MDBCol lg="2" className="d-none d-lg-block"></MDBCol>{" "}
        {/* Left padding column */}
        <MDBCol lg="8" md="12">
          {createPictureGrid()}
        </MDBCol>
        <MDBCol lg="2" className="d-none d-lg-block"></MDBCol>{" "}
        {/* Right padding column */}
      </MDBRow>
      <br />
      <h4>Fortschritt zurücksetzen</h4>
      <p>
        Gesamtfortschritt und Level auf null zurücksetzen?
        <br />
        Alle Medaillen werden entfernt. Freigeschaltete Avatare gehen verloren.
      </p>
      <MDBBtn color="danger" onClick={handleResetProgress}>
        Zurücksetzen
      </MDBBtn>
    </MDBContainer>
  );
}

export default Settings;
