import React, { useState, useEffect } from "react";
import { MDBContainer, MDBSpinner } from "mdb-react-ui-kit"; // Import MDBSpinner for loading animation

import axiosInstance from "../services/axiosConfig";

import PatternUnitCard from "../components/Units/PatternUnitCard";
import TheoryUnitCard from "../components/Units/TheoryUnitCard";
import MatrixUnitCard from "../components/Units/MatrixUnitCard";
import ArrayUnitCard from "../components/Units/ArrayUnitCard";
import RegressionUnitCard from "../components/Units/RegressionUnitCard";

function Units() {
  const [userData, setUserData] = useState(null);
  const [isLoading, setIsLoading] = useState(true); // State for loading animation

  useEffect(() => {
    Promise.all([
      new Promise((resolve) => setTimeout(resolve, 1000)), // 1-second delay
      axiosInstance.get("/api/userdata/me/"), // API request
    ])
      .then(([, response]) => {
        setUserData(response.data); // Update state with the fetched data
        console.log("Fetched User Data:", response.data);
      })
      .catch((error) => {
        console.error("Error fetching user data:", error);
      })
      .finally(() => setIsLoading(false)); // Stop loading animation
  }, []);

  if (isLoading) {
    return (
      <MDBContainer className="text-center">
        <MDBSpinner role="status" style={{ marginTop: "300px" }}>
          <span className="visually-hidden">Loading...</span>
        </MDBSpinner>
      </MDBContainer>
    );
  }

  return (
    <MDBContainer>
      <h2 className="mb-3">Lerneinheiten</h2>
      <TheoryUnitCard progress={userData?.progress_theory} />
      <MatrixUnitCard progress={userData?.progress_matrix} />
      <ArrayUnitCard progress={userData?.progress_array} />
      <PatternUnitCard progress={userData?.progress_pattern} />
      <RegressionUnitCard progress={userData?.progress_regression} />
    </MDBContainer>
  );
}

export default Units;
