import React, { useEffect } from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import Dashboard from "./pages/Dashboard";
import Settings from "./pages/Settings";
import Units from "./pages/Units";
import Theory from "./pages/Theory";
import Matrix from "./pages/Matrix";
import Array from "./pages/Array";
import Pattern from "./pages/Pattern";
import Regression from "./pages/Regression";
import Login from "./pages/Login";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  useLocation,
} from "react-router-dom";
import ProtectedRoute from "./ProtectedRoute";

// const originalConsoleError = console.error;

// console.error = (...args) => {
//   // Define the pattern to detect
//   const assertionErrorPattern =
//     /AssertionError@https:\/\/cdn.jsdelivr.net\/npm\/chai\/chai.js/;

//   // Check if the error message matches the pattern
//   if (assertionErrorPattern.test(args[0])) {
//     // Suppress the error message
//     // You can log it somewhere else or just ignore it
//     // For example: console.log("Suppressed error:", args[0]);
//   } else {
//     // For all other errors, use the original console.error behavior
//     originalConsoleError(...args);
//   }
// };

function App() {
  // useEffect(() => {
  //   // Setup the global error handler
  //   const errorHandler = (message, source, lineno, colno, error) => {
  //     console.error("Caught global error:", message);
  //     // Optionally, manage state or perform other actions
  //     return true; // Prevents the default browser error handling
  //   };

  //   window.onerror = errorHandler;

  //   // Cleanup function
  //   return () => {
  //     window.onerror = null;
  //   };
  // }, []); // Empty dependency array ensures this runs once on mount and cleanup on unmount

  // Place this code in a high-level component, like your App component

  return (
    <Router>
      <MainContent />
    </Router>
  );
}

function MainContent() {
  const location = useLocation();

  return (
    <>
      {location.pathname !== "/login" && <Navbar />}
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route
          path="/"
          element={
            <ProtectedRoute>
              <Dashboard />
            </ProtectedRoute>
          }
        />
        <Route
          path="/dashboard"
          element={
            <ProtectedRoute>
              <Dashboard />
            </ProtectedRoute>
          }
        />
        <Route
          path="/settings"
          element={
            <ProtectedRoute>
              <Settings />
            </ProtectedRoute>
          }
        />
        <Route
          path="/units"
          element={
            <ProtectedRoute>
              <Units />
            </ProtectedRoute>
          }
        />
        <Route
          path="/theory"
          element={
            <ProtectedRoute>
              <Theory />
            </ProtectedRoute>
          }
        />
        <Route
          path="/matrix"
          element={
            <ProtectedRoute>
              <Matrix />
            </ProtectedRoute>
          }
        />
        <Route
          path="/array"
          element={
            <ProtectedRoute>
              <Array />
            </ProtectedRoute>
          }
        />
        <Route
          path="/pattern"
          element={
            <ProtectedRoute>
              <Pattern />
            </ProtectedRoute>
          }
        />
        <Route
          path="/regression"
          element={
            <ProtectedRoute>
              <Regression />
            </ProtectedRoute>
          }
        />
      </Routes>
    </>
  );
}

export default App;
