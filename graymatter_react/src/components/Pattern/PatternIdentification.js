import React, { useState } from "react";
import {
  MDBInput,
  MDBBtn,
  MDBRadio,
  MDBRow,
  MDBCol,
  MDBIcon,
} from "mdb-react-ui-kit";

function PatternIdentification(props) {
  const {
    selectedOption,
    setSelectedOption,
    charInput,
    setCharInput,
    isCorrect,
    setIsCorrect,
  } = props;

  // const [selectedOption, setSelectedOption] = useState("");
  // const [charInput, setCharInput] = useState("");
  // const [isCorrect, setIsCorrect] = useState(null);

  const handleOptionChange = (event) => {
    setSelectedOption(event.target.value);
  };

  const handleCharInputChange = (event) => {
    setCharInput(event.target.value);
  };

  const checkCharInput = () => {
    const pattern = new RegExp("['\";#&]|(--)|(/\\*)|(\\*/)|(\\|)");
    const isValid = pattern.test(charInput);
    setIsCorrect(isValid);
  };

  return (
    <div className="mt-2 mb-4">
      <h4>Aufgabe 2: Pattern erkennen</h4>
      <p>
        <i>
          Es gibt ein spezifisches Sicherheitsproblem im Zusammenhang mit der
          Login-Funktion. Ihre Aufgabe ist es, dieses Hauptproblem zu
          identifizieren. Wählen Sie die Option, die am besten die Art des
          Sicherheitsrisikos beschreibt, das durch die Anwendung ermöglicht
          wird:
        </i>
      </p>
      <MDBRadio
        name="problemOptions"
        id="option1"
        label="Buffer Overflow"
        value="Buffer Overflow"
        inline
        onChange={handleOptionChange}
        checked={selectedOption === "Buffer Overflow"}
      />
      <MDBRadio
        name="problemOptions"
        id="option2"
        label="Cross-Site Scripting (XSS)"
        value="Cross-Site Scripting"
        inline
        onChange={handleOptionChange}
        checked={selectedOption === "Cross-Site Scripting"}
      />
      <MDBRadio
        name="problemOptions"
        id="option3"
        label="SQL Injection"
        value="SQL Injection"
        inline
        onChange={handleOptionChange}
        checked={selectedOption === "SQL Injection"}
      />
      <MDBRadio
        name="problemOptions"
        id="option4"
        label="DDoS Attack"
        value="DDoS Attack"
        inline
        onChange={handleOptionChange}
        checked={selectedOption === "DDoS Attack"}
      />

      <p className="mt-4">
        <i>Geben Sie ein Zeichen ein, das das Problem verursachen könnte:</i>
      </p>
      <MDBRow>
        <MDBCol size="auto">
          <MDBInput
            label="Charakter"
            id="charInput"
            type="text"
            value={charInput}
            onChange={handleCharInputChange}
            maxLength="1"
          />
        </MDBCol>
        <MDBCol size="auto">
          <MDBBtn
            size="sm"
            className="me-3"
            onClick={checkCharInput}
            disabled={charInput.length === 0}
          >
            Prüfen
          </MDBBtn>
          {isCorrect === true && (
            <MDBIcon
              size="lg"
              fas
              icon="check-circle"
              style={{ color: "green" }}
            />
          )}
          {isCorrect === false && (
            <MDBIcon
              size="lg"
              fas
              icon="times-circle"
              style={{ color: "red" }}
            />
          )}
        </MDBCol>
      </MDBRow>
    </div>
  );
}

export default PatternIdentification;
