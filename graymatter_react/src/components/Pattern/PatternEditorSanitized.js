import React, { useState, useEffect } from "react";
import AceEditor from "react-ace";

import "ace-builds/src-noconflict/mode-jsx";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/theme-terminal";

import BibLoginSanitized from "./BibLoginSanitized";

// Abruf des Codes von BibLoginSanitized.js für den Code Editor mittels Webpack
// eslint-disable-next-line import/no-webpack-loader-syntax
import bibLoginSanitizedContent from "!!raw-loader!./BibLoginSanitized.js";

import "./PatternEditor.css";

function PatternEditorSanitized() {
  // Speichern des BibLoginSanitized Codes in State für Code Editor
  const [code, setCode] = useState(bibLoginSanitizedContent);

  // Log Lines
  const [logLines, setLogLines] = useState([
    ["2023-11-06 14:05:22", "johndoe", "12345", "Invalid login"],
    ["2023-11-06 14:06:30", "johndoe", "admin123", "Invalid login"],
    [
      "2023-11-06 14:10:11",
      "janedoe",
      "' OR '1'='1 ",
      "SQL Injection detected",
    ],
    [
      "2023-11-06 14:10:45",
      "janedoe",
      "123' OR '1'='1' --",
      "SQL Injection detected",
    ],
    ["2023-11-06 14:25:30", "janedoe", "library", "Successful login"],
  ]);

  // Funktion zur Formattierung einer Log Line
  const formatLine = (timestamp, username, attemptedPassword, errorMessage) => {
    return `${timestamp.padEnd(20, " ")}| ${username.padEnd(
      12,
      " "
    )}| ${attemptedPassword.padEnd(20, " ")}| ${errorMessage}\n`;
  };

  // Log String bauen
  let logString = "--- Error Log: University Library Portal ---\n";
  logString +=
    "Timestamp            | Username    | Attempted Password  | Error Message\n";
  logString +=
    "---------------------------------------------------------------------------------------\n";

  logLines.forEach((line) => {
    logString += formatLine(line[0], line[1], line[2], line[3]);
  });

  logString +=
    "---------------------------------------------------------------------------------------";

  // Log State setzen
  const [Log, setLog] = useState(logString);

  // Funktion um weitere Log Lines hinzuzufügen
  const addLog = (username, password, message) => {
    const timestamp = new Date().toISOString().slice(0, 19).replace("T", " "); // Get current time in format "YYYY-MM-DD HH:MM:SS"
    // const errorMessage = message;

    const newLine = [timestamp, username, password, message];
    setLogLines((prevLogLines) => [...prevLogLines, newLine]);

    // Rebuild logString
    let newLogString = "--- Error Log: University Library Portal ---\n";
    newLogString +=
      "Timestamp            | Username    | Attempted Password  | Error Message\n";
    newLogString +=
      "---------------------------------------------------------------------------------------\n";

    [...logLines, newLine].forEach((line) => {
      newLogString += formatLine(line[0], line[1], line[2], line[3]);
    });

    newLogString +=
      "---------------------------------------------------------------------------------------";

    setLog(newLogString);
  };

  const [RenderedComponent, setRenderedComponent] = useState(null);

  useEffect(() => {
    try {
      const func = new Function("React", code);
      setRenderedComponent(() => () => func(React));
    } catch (error) {
      console.error(error);
    }
  }, [code]);

  return (
    <div className="container my-4">
      <div className="row mb-2">
        <div className="col-lg-8">
          <div className="form-group mt-2">
            <AceEditor
              style={{
                width: "100%",
                height: "650px",
                overflow: "auto",
              }}
              mode="jsx"
              theme="monokai"
              value={code}
              onChange={(newCode) => setCode(newCode)}
              name="codeEditor"
              editorProps={{ $blockScrolling: true }}
              fontSize={14}
              showPrintMargin={true}
              showGutter={true}
              highlightActiveLine={true}
              setOptions={{
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true,
                enableSnippets: false,
                showLineNumbers: true,
                tabSize: 2,
              }}
              readOnly={true} // Nicht editierbar
            />
          </div>
        </div>
        <div className="col-lg-4 d-flex justify-content-center">
          <div className="form-group smartphone-container">
            <div
              className="p-3"
              style={{ width: "290px", height: "590px", overflow: "auto" }}
            >
              <BibLoginSanitized addLog={addLog} />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <AceEditor
            style={{
              width: "100%",
              height: "240px",
              overflow: "auto",
            }}
            mode="text"
            theme="monokai"
            value={Log}
            onChange={(newLog) => setLog(newLog)}
            name="logEditor"
            editorProps={{ $blockScrolling: true }}
            fontSize={14}
            showPrintMargin={true}
            showGutter={true}
            highlightActiveLine={true}
            setOptions={{
              enableBasicAutocompletion: true,
              enableLiveAutocompletion: true,
              enableSnippets: false,
              showLineNumbers: true,
              tabSize: 2,
            }}
            readOnly={true} // Nicht editierbar
          />
        </div>
      </div>
    </div>
  );
}

export default PatternEditorSanitized;
