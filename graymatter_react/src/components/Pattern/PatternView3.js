import React from "react";
import PatternScenario from "./PatternScenario";
import PatternEditor from "./PatternEditor";
import PatternIdentification from "./PatternIdentification";

function PatternView3(props) {
  return (
    <div className={props.className}>
      <PatternScenario />
      <PatternEditor />
      <PatternIdentification
        selectedOption={props.selectedOption}
        setSelectedOption={props.setSelectedOption}
        charInput={props.charInput}
        setCharInput={props.setCharInput}
        isCorrect={props.isCorrect}
        setIsCorrect={props.setIsCorrect}
      />
    </div>
  );
}

export default PatternView3;
