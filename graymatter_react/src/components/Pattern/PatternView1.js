import React, { useEffect, useState } from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
} from "mdb-react-ui-kit";
import kursImage from "../../assets/images/kurs.png";
import "./PatternView1.css";

function PatternView1(props) {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    setLoaded(true);
  }, []);

  return (
    <div className={props.className}>
      <div className="row">
        {/* Left column */}
        <div className="col-12 col-lg-8">
          <div className="d-flex flex-column align-items-start">
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Definition</MDBCardTitle>
                <MDBCardText>
                  <i>Pattern-Testing</i> ist eine Software-Testmethode, die im
                  Rahmen des Gray-Box-Testings eingesetzt wird. Sie konzentriert
                  sich auf die{" "}
                  <strong>Identifizierung und Nutzung von Mustern</strong> im
                  Systemverhalten und in den Daten, um Probleme zu erkennen und
                  die Softwarequalität zu verbessern. Beim Pattern-Testing
                  verwenden die Tester bekannte Muster, wie z. B.
                  Benutzerverhalten, Eingabedatenformate oder Systemreaktionen,
                  um Code zu debuggen oder Testfälle zu erstellen. Diese Methode
                  ist besonders effektiv bei der Aufdeckung bestimmter Arten von
                  Fehlern, die aufgrund der identifizierten Muster{" "}
                  <strong>vorhersehbar </strong>
                  sind.
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards 0.5s" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Vorteile</MDBCardTitle>
                <MDBCardText>
                  <ul>
                    <li>
                      <strong>Prädiktive Fehlererkennung:</strong> Fehler, die
                      auf Mustern basieren, werden effizient erkannt, so dass
                      Tester bestimmte Fehlertypen, die häufig mit diesen
                      Mustern verbunden sind, vorhersehen und beheben können.
                    </li>
                    <li>
                      <strong>
                        Gezieltes Testen in Bereichen mit hohem Risiko:
                      </strong>{" "}
                      Durch das Erkennen und gezielte Testen von häufigen
                      Fehlermustern hilft Pattern-Testing, die Anstrengungen auf
                      die Bereiche der Anwendung zu konzentrieren, die besonders
                      fehleranfällig sind.
                    </li>
                    <li>
                      <strong>Ressourcenoptimierung:</strong> Optimiert die
                      Nutzung der Testressourcen durch Konzentration auf Muster,
                      anstatt alle möglichen Szenarien zu testen.
                    </li>
                  </ul>
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
          </div>
        </div>
        {/* Right column */}
        <div className="col-12 col-lg-4 d-none d-lg-flex justify-content-center align-items-center">
          <img src={kursImage} alt="Kurs" className="img-fluid" />
        </div>
      </div>
    </div>
  );
}

export default PatternView1;
