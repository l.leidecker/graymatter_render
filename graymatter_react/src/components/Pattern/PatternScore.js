import React from "react";
import { useNavigate } from "react-router-dom";
import {
  MDBModal,
  MDBModalDialog,
  MDBModalContent,
  MDBModalHeader,
  MDBModalTitle,
  MDBModalBody,
  MDBModalFooter,
  MDBBtn,
  MDBRow,
  MDBCol,
  MDBIcon,
} from "mdb-react-ui-kit";

import { Link } from "react-router-dom";

import "./PatternScore.css";

import medalZeroImg from "../../assets/images/medal0.png";
import medalFirstImg from "../../assets/images/medal1.png";
import medalSecondImg from "../../assets/images/medal2.png";
import medalThirdImg from "../../assets/images/medal3.png";

function PatternScore({ isOpen, scores }) {
  const navigate = useNavigate(); // Using the useNavigate hook
  const {
    scoreExercise1,
    scoreExercise2,
    scoreExercise3,
    overallScore,
    finalTime,
  } = scores;

  let medalImg, resultMessage, medalClass;
  if (overallScore === 6) {
    medalImg = medalFirstImg;
    resultMessage = "Bestanden!";
    medalClass = "medal-glow gold";
  } else if (overallScore === 5) {
    medalImg = medalSecondImg;
    resultMessage = "Bestanden!";
    medalClass = "medal-glow silver";
  } else if (overallScore === 4 || overallScore === 3) {
    medalImg = medalThirdImg;
    resultMessage = "Bestanden!";
    medalClass = "medal-glow bronze";
  } else {
    medalImg = medalZeroImg;
    resultMessage = "Nicht bestanden";
    medalClass = ""; // No glow for zero score
  }

  // Convert finalTime to minutes and seconds for display
  const finalMinutes = String(Math.floor(finalTime / 60)).padStart(2, "0");
  const finalSeconds = String(finalTime % 60).padStart(2, "0");

  const handleBackToUnits = () => {
    navigate("/units"); // Navigate to '/units' route
  };

  const showAlert = overallScore === 6;

  return (
    <MDBModal show={isOpen} tabIndex="-1" staticBackdrop>
      <MDBModalDialog size="md">
        <MDBModalContent>
          <MDBModalHeader>
            <MDBModalTitle>Lerneinheit abgeschlossen</MDBModalTitle>
          </MDBModalHeader>
          <MDBModalBody>
            <MDBRow>
              {/* Left Column for Score Details */}
              <MDBCol size="7">
                <p>
                  Zeit: {finalMinutes}:{finalSeconds}
                </p>
                <p>
                  <i>Aufgabe 1:</i> {scoreExercise1}/3
                </p>
                <p>
                  <i>Aufgabe 2:</i> {scoreExercise2}/2
                </p>
                <p>
                  <i>Aufgabe 3:</i> {scoreExercise3}/1
                </p>
                <br />
                <p>
                  <strong>Gesamtergebnis: {overallScore}/6</strong>
                </p>
              </MDBCol>

              {/* Right Column for Medal Image and Text */}
              <MDBCol
                size="5"
                className="d-flex flex-column align-items-center justify-content-center"
              >
                <div className={medalClass}>
                  <img src={medalImg} alt="Medal" className="img-fluid" />
                </div>
                <p className="mt-2">
                  <strong>{resultMessage}</strong>
                </p>
              </MDBCol>
            </MDBRow>
            {showAlert && (
              <MDBRow>
                <MDBCol size="12">
                  <div className="alert alert-success">
                    <MDBIcon fas icon="trophy" size="lg" className="me-3" />
                    Neues <Link to="/settings">Profilbild</Link> freigeschaltet!
                  </div>
                </MDBCol>
              </MDBRow>
            )}
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="success" onClick={handleBackToUnits}>
              Zurückkehren
            </MDBBtn>
          </MDBModalFooter>
        </MDBModalContent>
      </MDBModalDialog>
    </MDBModal>
  );
}

export default PatternScore;
