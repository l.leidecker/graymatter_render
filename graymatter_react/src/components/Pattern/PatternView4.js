import React from "react";
import PatternScenario from "./PatternScenario";
import PatternEditorSanitized from "./PatternEditorSanitized";
import PatternDebug from "./PatternDebug";

function PatternView4(props) {
  return (
    <div className={props.className}>
      <PatternScenario />
      <PatternEditorSanitized />
      <PatternDebug
        debugCorrect={props.debugCorrect}
        setDebugCorrect={props.setDebugCorrect}
      />
    </div>
  );
}

export default PatternView4;
