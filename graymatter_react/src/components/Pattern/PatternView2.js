import React, { useState } from "react";
import { MDBRadio, MDBCard, MDBCardBody } from "mdb-react-ui-kit";

import questionBook from "../../assets/images/question_book.png";

function PatternView2(props) {
  // States for the selected answers
  // Props (from Pattern.js) for state and handlers
  const {
    selectedQuestion1,
    setSelectedQuestion1,
    selectedQuestion2,
    setSelectedQuestion2,
    selectedQuestion3,
    setSelectedQuestion3,
  } = props;

  // Handlers for changed selection
  const handleQuestion1Change = (value) => {
    setSelectedQuestion1(value);
  };

  const handleQuestion2Change = (value) => {
    setSelectedQuestion2(value);
  };

  const handleQuestion3Change = (value) => {
    setSelectedQuestion3(value);
  };

  return (
    <div className={props.className}>
      <div className="row">
        {/* Left Column */}
        <div className="col-12 col-lg-8">
          <MDBCard className="mb-3">
            <MDBCardBody>
              {/* Question 1 */}
              <p>
                <strong>
                  Welches der folgenden Szenarien veranschaulicht am besten den
                  Einsatz von Pattern-Testing zur Identifizierung von
                  Systemleistungsproblemen?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question1"
                  id="radio1A"
                  label="A) Analyse der Antwortzeiten für bestimmte Datenbankabfragen unter hoher Last"
                  onChange={() => handleQuestion1Change("A")}
                  checked={selectedQuestion1 === "A"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1B"
                  label="B) Bewertung der Kompatibilität von Software auf verschiedenen Betriebssystemen."
                  onChange={() => handleQuestion1Change("B")}
                  checked={selectedQuestion1 === "B"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1C"
                  label="C) Überprüfung der Rechtschreibung und Grammatik im Benutzerhandbuch der Anwendung."
                  onChange={() => handleQuestion1Change("C")}
                  checked={selectedQuestion1 === "C"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1D"
                  label="D) Überprüfung der rechtlichen Bedingungen für die Nutzung der Software."
                  onChange={() => handleQuestion1Change("D")}
                  checked={selectedQuestion1 === "D"}
                />
              </div>

              {/* Question 2 */}
              <p>
                <strong>
                  Welche Art von Fehlern wird mit Pattern-Testing am
                  effizientesten erkannt?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question2"
                  id="radio2A"
                  label="A) Ein zufälliger Fehler, der ohne ein bestimmtes Muster auftritt."
                  onChange={() => handleQuestion2Change("A")}
                  checked={selectedQuestion2 === "A"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2B"
                  label="B) Ein wiederkehrender Fehler, der jedes Mal auftritt, wenn ein bestimmter Datensatz verarbeitet wird"
                  onChange={() => handleQuestion2Change("B")}
                  checked={selectedQuestion2 === "B"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2C"
                  label="C) Probleme mit ästhetischen Elementen der Benutzungsoberfläche."
                  onChange={() => handleQuestion2Change("C")}
                  checked={selectedQuestion2 === "C"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2D"
                  label="D) Probleme mit der Konformität der Softwaredokumentation."
                  onChange={() => handleQuestion2Change("D")}
                  checked={selectedQuestion2 === "D"}
                />
              </div>

              {/* Question 3 */}
              <p>
                <strong>
                  Welches der folgenden Muster würde man im Zusammenhang mit dem
                  Pattern-Testing für Sicherheitslücken als bekannt ansehen?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question3"
                  id="radio3A"
                  label="A) Die Vorliebe von Nutzern für bestimmte Oberflächenfarben."
                  onChange={() => handleQuestion3Change("A")}
                  checked={selectedQuestion3 === "A"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3B"
                  label="B) Die Tendenz des Systems, an Montagen langsamer zu werden."
                  onChange={() => handleQuestion3Change("B")}
                  checked={selectedQuestion3 === "B"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3C"
                  label="C) Bestimmte Eingabemuster, die häufig bei SQL-Injection-Angriffen verwendet werden"
                  onChange={() => handleQuestion3Change("C")}
                  checked={selectedQuestion3 === "C"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3D"
                  label="D) Die Schwankungen des Batterieverbrauchs bei mobilen Anwendungen."
                  onChange={() => handleQuestion3Change("D")}
                  checked={selectedQuestion3 === "D"}
                />
              </div>
            </MDBCardBody>
          </MDBCard>
        </div>
        {/* Right Column */}
        <div className="col-12 col-lg-4 d-none d-lg-flex justify-content-center align-items-center">
          <img src={questionBook} alt="Book" className="img-fluid" />
        </div>
      </div>
    </div>
  );
}

export default PatternView2;
