import React, { useState } from "react";
import { handleServerLogin } from "./BibServer"; // Import handleServerLogin function

function BibLogin(props) {
  // State variables
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  // Login function
  const initiateLogin = async () => {
    setIsLoading(true);
    setMessage(""); // Reset status message

    try {
      // Call to handleServerLogin with username and password
      const status = await handleServerLogin(username, password, props.addLog);

      // Update the login status
      handleLoginStatus(status);
    } catch (error) {
      setMessage(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  // Callback function for updating the login status
  const handleLoginStatus = (status) => {
    let displayMessage;

    // Determine display message based on server response
    if (status === "Login Successful") {
      displayMessage = "Successful login";
    } else {
      // For both Login Failed and SQL Injection, show "Invalid login" to the user
      displayMessage = "Invalid login";
    }

    // Set the generic status message to be displayed on the frontend
    setMessage(displayMessage);
  };

  return (
    <div className="container">
      <div className="justify-content-center align-items-center">
        <div>
          <h3 className="text-center my-5">Bibliotheksportal</h3>
          <div className="form-group mb-2">
            <input
              type="text"
              className="form-control"
              placeholder="Username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              autoComplete="off"
            />
          </div>
          <div className="form-group mb-3">
            <input
              type="password"
              className="form-control"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              autoComplete="off"
            />
          </div>
          <button
            className="btn btn-primary btn-block"
            onClick={initiateLogin}
            disabled={isLoading}
          >
            {isLoading ? "Loading..." : "Login"}
          </button>
          {message && (
            <div
              className={`alert ${
                message === "Login Successful"
                  ? "alert-success"
                  : "alert-danger"
              } mt-3`}
              role="alert"
            >
              {message}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default BibLogin;
