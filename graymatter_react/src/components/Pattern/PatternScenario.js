import React from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCardText,
} from "mdb-react-ui-kit";

function PatternScenario() {
  return (
    <MDBCard>
      <MDBCardHeader>Universitäres Authentifizierungssystem</MDBCardHeader>
      <MDBCardBody>
        <MDBCardText>
          Sie sind Software-Tester für das digitale Bibliotheksportal einer
          Universität. Die IT-Abteilung hat in letzter Zeit vermehrt
          unautorisierte Anmeldeversuche festgestellt. Die Universität hat
          bereits in der Vergangenheit Sicherheitslücken im Zusammenhang mit
          ihrem Anmelde- und Authentifizierungssystem festgestellt. Ihre Aufgabe
          ist es, das Anmelde- und Authentifizierungssystem auf mögliche
          Schwachstellen zu untersuchen.
          <br />
          <br />
          <i>
            Nachfolgend haben Sie Einsicht in die Anwendung und den Quellcode
            der Authentifizierungsklasse sowie die Error-Logs des
            Authentifzierungsservers. Die Implementierung des
            Authentifizierzungsservers ist ihnen verborgen.
          </i>
        </MDBCardText>
      </MDBCardBody>
    </MDBCard>
  );
}

export default PatternScenario;
