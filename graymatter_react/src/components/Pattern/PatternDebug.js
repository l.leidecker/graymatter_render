import React, { useState } from "react";
import AceEditor from "react-ace";
import { MDBBtn, MDBIcon } from "mdb-react-ui-kit";

const initialCode = `
  function sanitize(input) {
    // Code here
    return input
    .replace(/'/g, "") 
    .replace(/"/g, "") 
    .replace(/#/g, "") 
  }
`;

function PatternDebug(props) {
  const [code, setCode] = useState(initialCode);
  const [alertMessage, setAlertMessage] = useState("");
  const [alertType, setAlertType] = useState("");

  const { debugCorrect, setDebugCorrect } = props;

  const handleCodeChange = (newCode) => {
    setCode(newCode);
  };

  const handleCheck = () => {
    try {
      const userFunction = new Function(`return (${code})`)();
      const testString = "allowed'\";#&--/*|allowed";
      const sanitizedString = userFunction(testString);

      const retainsAllowed = sanitizedString.includes("allowed");
      const unallowedChars = ["'", '"', ";", "#", "&", "--", "/", "*", "|"];
      const isSanitizedCorrectly = unallowedChars.some((char) => {
        return testString.includes(char) && !sanitizedString.includes(char);
      });

      if (retainsAllowed && isSanitizedCorrectly) {
        setAlertMessage(
          "Die Antwort ist richtig. Die Eingabe wurde bereinigt."
        );
        setAlertType("alert-success");
        setDebugCorrect(true);
      } else {
        setAlertMessage(
          "Die Funktion hat die Eingabe nicht ordnungsgemäß bereinigt."
        );
        setAlertType("alert-warning");
        setDebugCorrect(false);
      }
    } catch (error) {
      console.error(error);
      setAlertMessage("Der Code weist syntaktische Fehler auf.");
      setAlertType("alert-danger");
      setDebugCorrect(false);
    }
  };

  const handleReset = () => {
    setCode(initialCode); // Code zu ursprünglichem Zustand zurücksetzen
  };

  return (
    <div className="mt-2 mb-4">
      <h4>Aufgabe 3: Pattern debuggen</h4>
      <p>
        <i>
          Die Authentifizierungsklasse oben wurde um zwei Variablen
          (sanitizedUsername, sanitizesPassword) und eine Funktion (function
          sanitize(input)) ergänzt. Programmieren Sie die Funktion zur
          Frontend-seitigen Behebung des in Aufgabe 2 identifizierten Problems.
          Führen Sie die Funktion einmal erfolgreich aus.
        </i>
      </p>

      <div className="d-flex align-items-center">
        <MDBBtn color="success" onClick={handleCheck} className="me-2 mb-3">
          <MDBIcon fas icon="play" className="me-2" />
          Ausführen
        </MDBBtn>
        <MDBBtn
          onClick={handleReset}
          outline
          color="danger"
          className="me-2 mb-3"
        >
          <MDBIcon fas icon="stop" className="me-2" />
          Zurücksetzen
        </MDBBtn>
        {alertMessage && (
          <div className={`alert ${alertType} mb-3`} role="alert">
            {alertMessage}
          </div>
        )}
      </div>

      <AceEditor
        style={{ width: "100%", height: "200px", overflow: "auto" }}
        mode="jsx"
        theme="monokai"
        value={code}
        onChange={handleCodeChange}
        name="codeEditor"
        editorProps={{ $blockScrolling: true }}
        fontSize={14}
        showPrintMargin={true}
        showGutter={true}
        highlightActiveLine={true}
        setOptions={{
          enableBasicAutocompletion: false,
          enableLiveAutocompletion: false,
          enableSnippets: false,
          showLineNumbers: true,
          tabSize: 2,
        }}
      />
    </div>
  );
}

export default PatternDebug;
