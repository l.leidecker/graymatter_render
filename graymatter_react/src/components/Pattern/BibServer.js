import PropTypes from "prop-types";

// Funktion die SQL Injection Pattern erkennt
const detectSQLInjection = (str) => {
  const pattern = new RegExp("['\";#&]|(--)|(/\\*)|(\\*/)|(\\|)");
  return pattern.test(str);
};

// Simuliere serverseitiges Login-Logik
export const handleServerLogin = async (username, password, addLog) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      let result;
      if (detectSQLInjection(username) || detectSQLInjection(password)) {
        result = "SQL Injection Detected";
      } else if (
        (username === "admin" && password === "1234") ||
        (username === "janedoe" && password === "library")
      ) {
        result = "Login Successful";
      } else {
        result = "Login Failed";
      }

      // Log the login attempt using addLog
      addLog(username, password, result);

      resolve(result);
    }, 1000); // Simulated delay
  });
};

// Prop types validation
handleServerLogin.propTypes = {
  username: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
};
