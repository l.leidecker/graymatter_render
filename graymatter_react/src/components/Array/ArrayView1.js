import React, { useEffect, useState } from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
} from "mdb-react-ui-kit";
import kursImage from "../../assets/images/kurs.png";
import "./ArrayView1.css";

function ArrayView1(props) {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    setLoaded(true);
  }, []);

  return (
    <div className={props.className}>
      <div className="row">
        {/* Linke Spalte */}
        <div className="col-12 col-lg-8">
          <div className="d-flex flex-column align-items-start">
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Definition</MDBCardTitle>
                <MDBCardText>
                  <i>Orthogonal-Array-Tests (OAT)</i> sind eine Testmethode, bei
                  der unabhängige Daten oder Entitäten{" "}
                  <strong>paarweise</strong> als Testeingabeparameter{" "}
                  <strong>kombiniert</strong> verwendet werden. Hierdurch wird
                  eine effiziente Prüfung
                  <strong> aller Variablenkombinationen </strong>
                  ermöglicht, um die Testabdeckung trotz begrenzter Anzahl von
                  Testfällen zu erhöhen.
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards 0.5s" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Warum OAT?</MDBCardTitle>
                <MDBCardText>
                  <ul>
                    <li>
                      <strong>Effizienz:</strong> Testet mehrere Faktoren
                      gleichzeitig und reduziert so die Anzahl der Testfälle.
                    </li>
                    <li>
                      <strong>Abdeckungsgrad:</strong> Gewährleistet eine
                      angemessene Testabdeckung für alle möglichen Kombinationen
                      von Variablen.
                    </li>
                  </ul>
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards 1s" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Wichtige Begriffe</MDBCardTitle>
                <MDBCardText>
                  <ul>
                    <li>
                      <strong>Faktor:</strong> Eine Variable im Testszenario.
                    </li>
                    <li>
                      <strong>Level:</strong> Die maximale Anzahl von Werten,
                      die eine Variable annehmen kann.
                    </li>
                    <li>
                      <strong>Orthogonal Array (OA):</strong> Eine Matrix, bei
                      der jede Zeile für einen Testfall und jede Spalte für
                      einen Faktor steht.
                    </li>
                  </ul>
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
          </div>
        </div>
        {/* Rechte Spalte */}
        <div className="col-12 col-lg-4 d-none d-lg-flex justify-content-center align-items-center">
          <img src={kursImage} alt="Kurs" className="img-fluid" />
        </div>
      </div>
    </div>
  );
}

export default ArrayView1;
