import React, { useState } from "react";

function ECommerce() {
  const [brand, setBrand] = useState("Apple");
  const [color, setColor] = useState("Red");
  const [price, setPrice] = useState("Low");

  return (
    <div className="container mt-4">
      <h3 className="mb-5">E-Buy Commerce</h3>
      <p>Konfigurieren Sie ihr Produkt.</p>
      <div className="form-group">
        <label>Marke Auswählen:</label>
        <select
          className="form-control"
          onChange={(e) => setBrand(e.target.value)}
        >
          <option value="Apple">Apple</option>
          <option value="Samsung">Samsung</option>
          <option value="Sony">Sony</option>
        </select>
      </div>

      <div className="form-group">
        <label>Farbe auswählen:</label>
        <select
          className="form-control"
          onChange={(e) => setColor(e.target.value)}
        >
          <option value="Red">Red</option>
          <option value="Green">Green</option>
          <option value="Blue">Blue</option>
        </select>
      </div>

      <div className="form-group">
        <label>Preisspanne auswählen:</label>
        <select
          className="form-control"
          onChange={(e) => setPrice(e.target.value)}
        >
          <option value="Low">Low</option>
          <option value="Medium">Medium</option>
          <option value="High">High</option>
        </select>
      </div>

      <p className="mt-3">
        Ihre gewählte Marke: <strong>{brand}</strong>, Farbe:{" "}
        <strong>{color}</strong>, and Preisspanne: <strong>{price}</strong>.
      </p>
    </div>
  );
}

export default ECommerce;
