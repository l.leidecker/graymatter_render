import React, { useState, useEffect } from "react";
import {
  MDBRange,
  MDBTable,
  MDBTableHead,
  MDBTableBody,
  MDBDropdown,
  MDBDropdownMenu,
  MDBDropdownToggle,
  MDBDropdownItem,
  MDBInput,
} from "mdb-react-ui-kit";

// Props destructered von der Parent Komponente ArrayView4
// States und Funktionen gespeichert in Parent Komponente Array.js
function ArrayTable({
  factorsCount,
  setFactorsCount,
  levelsCount,
  setLevelsCount,
  combinationCount,
  setCombinationCount,
  arrayLength,
  setArrayLength,
  dropdownValues,
  setDropdownValues,
}) {
  const factors = [
    ["Apple", "Samsung", "Sony"],
    ["Red", "Green", "Blue"],
    ["Low", "Medium", "High"],
  ];

  const handleDropdownChange = (row, col, value) => {
    let updatedValues = [...dropdownValues];
    if (!updatedValues[row]) {
      updatedValues[row] = [];
    }
    updatedValues[row][col] = value;
    setDropdownValues(updatedValues);
  };

  useEffect(() => {
    // Array kürzen damit dropdownValue arrayLength matched
    setDropdownValues((prevValues) => {
      if (arrayLength < prevValues.length) {
        return prevValues.slice(0, arrayLength);
      } else if (arrayLength > prevValues.length) {
        return [
          ...prevValues,
          ...Array(arrayLength - prevValues.length).fill([]),
        ];
      } else {
        return prevValues;
      }
    });
  }, [arrayLength]);

  const logData = () => {
    console.log("Array Length:", arrayLength);
    console.log("Table Content:", dropdownValues);
  };

  return (
    <div className="mt-2">
      <h4>Aufgabe 2: OAT konstruieren</h4>
      <p>
        Identifizieren Sie im Quellcode die Faktoren und Level und bestimmen Sie
        auf dieser Grundlage alle Testfälle durch Konstruktion eines
        orthogonalen Arrays.
      </p>
      <p style={{ display: "flex", alignItems: "center", gap: "10px" }}>
        <i>Wie viele Faktoren hat die Klasse?</i>
        <div style={{ maxWidth: "200px" }}>
          <MDBInput
            value={factorsCount}
            onChange={(e) => setFactorsCount(Number(e.target.value))}
            label="Faktoren"
            id="factorsCount"
            type="number"
          />
        </div>
      </p>

      <p
        style={{ display: "flex", alignItems: "center", gap: "10px" }}
        className="mt-3"
      >
        <i>Wie viele Level hat die Klasse?</i>
        <div style={{ maxWidth: "200px" }}>
          <MDBInput
            value={levelsCount}
            onChange={(e) => setLevelsCount(Number(e.target.value))}
            label="Level"
            id="levelsCount"
            type="number"
          />
        </div>
      </p>

      <p
        style={{ display: "flex", alignItems: "center", gap: "10px" }}
        className="mt-3"
      >
        <i>
          Wie hoch ist die Anzahl an Kombinationsmöglichkeiten ohne orthogonale
          Arrays?
        </i>
        <div style={{ maxWidth: "200px" }}>
          <MDBInput
            value={combinationCount}
            onChange={(e) => setCombinationCount(Number(e.target.value))}
            label="Kombinationen"
            id="combinationCount"
            type="number"
          />
        </div>
      </p>

      <br />

      <div style={{ display: "flex", alignItems: "center", gap: "10px" }}>
        <label className="form-label" style={{ flexShrink: 0 }}>
          Array-Länge L{arrayLength}
        </label>
        <div style={{ flexGrow: 1, marginLeft: "10px" }}>
          <MDBRange
            value={arrayLength}
            min="1"
            max="20"
            id="arrayLengthSlider"
            onChange={(e) => setArrayLength(Number(e.target.value))}
          />
        </div>
      </div>

      <h5 className="mt-2">Bestimmen Sie alle Testfälle:</h5>

      <MDBTable>
        <MDBTableHead>
          <tr>
            <th style={{ textAlign: "center", verticalAlign: "middle" }}>
              Testfälle
            </th>
            <th style={{ textAlign: "center", verticalAlign: "middle" }}>
              Faktor 1
            </th>
            <th style={{ textAlign: "center", verticalAlign: "middle" }}>
              Faktor 2
            </th>
            <th style={{ textAlign: "center", verticalAlign: "middle" }}>
              Faktor 3
            </th>
          </tr>
        </MDBTableHead>
        <MDBTableBody>
          {[...Array(arrayLength)].map((_, row) => (
            <tr key={row}>
              <td style={{ textAlign: "center", verticalAlign: "middle" }}>
                {row + 1}
              </td>
              {factors.map((factorOptions, col) => (
                <td
                  key={`${row}-${col}`} // Unique key combining row and column
                  style={{ textAlign: "center", verticalAlign: "middle" }}
                >
                  <MDBDropdown>
                    <MDBDropdownToggle
                      size="sm"
                      caret
                      color="primary"
                      style={{ width: "100px" }}
                    >
                      {dropdownValues[row] && dropdownValues[row][col]
                        ? dropdownValues[row][col]
                        : "Select"}
                    </MDBDropdownToggle>
                    <MDBDropdownMenu>
                      {factorOptions.map((option) => (
                        <MDBDropdownItem
                          key={option}
                          link
                          childTag="button"
                          onClick={(e) => {
                            e.preventDefault();
                            handleDropdownChange(row, col, option);
                          }}
                        >
                          {option}
                        </MDBDropdownItem>
                      ))}
                    </MDBDropdownMenu>
                  </MDBDropdown>
                </td>
              ))}
            </tr>
          ))}
        </MDBTableBody>
      </MDBTable>
    </div>
  );
}

export default ArrayTable;
