import React from "react";
import ArrayScenario from "./ArrayScenario";
import ArrayEditor from "./ArrayEditor";
import ArrayTable from "./ArrayTable";

function ArrayView4(props) {
  return (
    <div className={props.className}>
      <ArrayScenario />
      <ArrayEditor />
      <ArrayTable
        factorsCount={props.factorsCount}
        setFactorsCount={props.setFactorsCount}
        levelsCount={props.levelsCount}
        setLevelsCount={props.setLevelsCount}
        combinationCount={props.combinationCount}
        setCombinationCount={props.setCombinationCount}
        arrayLength={props.arrayLength}
        setArrayLength={props.setArrayLength}
        dropdownValues={props.dropdownValues}
        setDropdownValues={props.setDropdownValues}
      />
    </div>
  );
}

export default ArrayView4;
