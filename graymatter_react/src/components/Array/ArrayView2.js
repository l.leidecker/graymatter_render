import React, { useState } from "react";
import { MDBRadio, MDBCard, MDBCardBody } from "mdb-react-ui-kit";

import questionBook from "../../assets/images/question_book.png";

function ArrayView2(props) {
  // States der ausgewählten Antworten
  const {
    selectedQuestion1,
    setSelectedQuestion1,
    selectedQuestion2,
    setSelectedQuestion2,
    selectedQuestion3,
    setSelectedQuestion3,
    selectedQuestion4,
    setSelectedQuestion4,
  } = props;

  // Handler für veränderte Auswahl
  const handleQuestion1Change = (value) => {
    setSelectedQuestion1(value);
  };

  const handleQuestion2Change = (value) => {
    setSelectedQuestion2(value);
  };

  const handleQuestion3Change = (value) => {
    setSelectedQuestion3(value);
  };

  const handleQuestion4Change = (value) => {
    setSelectedQuestion4(value);
  };

  return (
    <div className={props.className}>
      <div className="row">
        {/* Linke Spalte */}
        <div className="col-12 col-lg-8">
          <MDBCard className="mb-3">
            <MDBCardBody>
              {/* Question 1 */}
              <p>
                <strong>
                  Was wird hauptsächlich durch Orthogonal-Array-Testing
                  erreicht?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question1"
                  id="radio1A"
                  label="A) Validierung der Benutzeroberfläche"
                  onChange={() => handleQuestion1Change("A")}
                  checked={selectedQuestion1 === "A"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1B"
                  label="B) Effiziente Testabdeckung von Variablenkombinationen."
                  onChange={() => handleQuestion1Change("B")}
                  checked={selectedQuestion1 === "B"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1C"
                  label="C) Überprüfung der Datenbankintegrität"
                  onChange={() => handleQuestion1Change("C")}
                  checked={selectedQuestion1 === "C"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1D"
                  label="D) Testen der Netzwerkleistung"
                  onChange={() => handleQuestion1Change("D")}
                  checked={selectedQuestion1 === "D"}
                />
              </div>
              {/* Question 2 */}
              <p>
                <strong>
                  Welcher der folgenden Faktoren wäre KEIN guter Anwendungsfall
                  für OAT?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question2"
                  id="radio2A"
                  label="A) Benutzeroberflächen-Layout"
                  onChange={() => handleQuestion2Change("A")}
                  checked={selectedQuestion2 === "A"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2B"
                  label="B) Algorithmen mit vielen unabhängigen Variablen."
                  onChange={() => handleQuestion2Change("B")}
                  checked={selectedQuestion2 === "B"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2C"
                  label="C) Hardware-Konfigurationsoptionen"
                  onChange={() => handleQuestion2Change("C")}
                  checked={selectedQuestion2 === "C"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2D"
                  label="D) Kombination verschiedener Eingabeformate"
                  onChange={() => handleQuestion2Change("D")}
                  checked={selectedQuestion2 === "D"}
                />
              </div>

              {/* Question 3 */}
              <p>
                <strong>Was ist ein wichtiger Vorteil von OAT?</strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question3"
                  id="radio3A"
                  label="A) Kann ohne Testplan verwendet werden"
                  onChange={() => handleQuestion3Change("A")}
                  checked={selectedQuestion3 === "A"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3B"
                  label="B) Ist ausschließlich für Regressionstests geeignet"
                  onChange={() => handleQuestion3Change("B")}
                  checked={selectedQuestion3 === "B"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3C"
                  label="C) Reduziert die Anzahl der erforderlichen Testfälle bei gleichzeitiger Erhöhung der Abdeckung."
                  onChange={() => handleQuestion3Change("C")}
                  checked={selectedQuestion3 === "C"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3D"
                  label="D) Spezialisiert auf Performance-Tests"
                  onChange={() => handleQuestion3Change("D")}
                  checked={selectedQuestion3 === "D"}
                />
              </div>

              {/* Question 4 */}
              <p>
                <strong>
                  Was wird bei der Orthogonal-Array-Testing-Methode
                  typischerweise in den Zeilen und Spalten einer Matrix
                  repräsentiert?
                </strong>
              </p>
              <div>
                <MDBRadio
                  name="question4"
                  id="radio4A"
                  label="A) Jede Zeile steht für einen Testfall, jede Spalte für eine Variable."
                  onChange={() => handleQuestion4Change("A")}
                  checked={selectedQuestion4 === "A"}
                />
                <MDBRadio
                  name="question4"
                  id="radio4B"
                  label="B) Jede Zeile steht für eine Variable, jede Spalte für einen Testfall"
                  onChange={() => handleQuestion4Change("B")}
                  checked={selectedQuestion4 === "B"}
                />
                <MDBRadio
                  name="question4"
                  id="radio4C"
                  label="C) Jede Zeile steht für einen Testergebnis, jede Spalte für einen Testfall"
                  onChange={() => handleQuestion4Change("C")}
                  checked={selectedQuestion4 === "C"}
                />
                <MDBRadio
                  name="question4"
                  id="radio4D"
                  label="D) Jede Zeile steht für eine Testeingabe, jede Spalte für eine Testausgabe"
                  onChange={() => handleQuestion4Change("D")}
                  checked={selectedQuestion4 === "D"}
                />
              </div>
            </MDBCardBody>
          </MDBCard>
        </div>
        {/* Rechte Spalte */}
        <div className="col-12 col-lg-4 d-none d-lg-flex justify-content-center align-items-center">
          <img src={questionBook} alt="Buch" className="img-fluid" />
        </div>
      </div>
    </div>
  );
}

export default ArrayView2;
