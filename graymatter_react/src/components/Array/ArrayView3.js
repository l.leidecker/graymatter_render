import React from "react";

function ArrayView3(props) {
  return (
    <div className={props.className}>
      <h4>Beispiel für eine OAT-Matrix</h4>
      <p>
        <i>
          Schauen Sie sich die Lektion über die Konstruktion von OAT-Matrizen an
          (0:53 bis 2:28).
        </i>
      </p>
      {/* Centered div wrapper */}
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {/* Embed YouTube video starting at 53 seconds and ending at 148 seconds */}
        <iframe
          width="800" // Increased width
          height="450" // Increased height
          src="https://www.youtube.com/embed/yXPZLfrb_JU?start=53&end=150"
          title="YouTube video player"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        ></iframe>
      </div>
      {/* rest of the component content */}
    </div>
  );
}

export default ArrayView3;
