import React, { useState, useEffect } from "react";
import AceEditor from "react-ace";

import "ace-builds/src-noconflict/mode-jsx";
import "ace-builds/src-noconflict/theme-monokai";

import ECommerce from "./ECommerce";

import "./ArrayEditor.css";

function ArrayEditor() {
  const [code, setCode] = useState(`
  import React, { useState } from 'react';

  function ECommerce {
    const [brand, setBrand] = useState('Apple');
    const [color, setColor] = useState('Red');
    const [price, setPrice] = useState('Low');
  
    return (
      <div className="container mt-4">
        <div className="form-group">
          <label>Select Brand:</label>
          <select className="form-control" onChange={(e) => setBrand(e.target.value)}>
            <option value="Apple">Apple</option>
            <option value="Samsung">Samsung</option>
            <option value="Sony">Sony</option>
          </select>
        </div>
  
        <div className="form-group">
          <label>Select Color:</label>
          <select className="form-control" onChange={(e) => setColor(e.target.value)}>
            <option value="Red">Red</option>
            <option value="Green">Green</option>
            <option value="Blue">Blue</option>
          </select>
        </div>
  
        <div className="form-group">
          <label>Select Price Range:</label>
          <select className="form-control" onChange={(e) => setPrice(e.target.value)}>
            <option value="Low">Low</option>
            <option value="Medium">Medium</option>
            <option value="High">High</option>
          </select>
        </div>
  
        <p className="mt-3">You selected Brand: <strong>{brand}</strong>, Color: <strong>{color}</strong>, and Price Range: <strong>{price}</strong>.</p>
      </div>
    );
  }
  
  export default ECommerce;
  
`);
  const [RenderedComponent, setRenderedComponent] = useState(null);

  useEffect(() => {
    try {
      const func = new Function("React", code);
      setRenderedComponent(() => () => func(React));
    } catch (error) {
      console.error(error);
    }
  }, [code]);

  return (
    <div className="container my-4">
      <div className="row">
        <div className="col-lg-8">
          <div className="form-group mt-2">
            <AceEditor
              style={{
                width: "100%",
                height: "650px",
                overflow: "auto",
              }}
              mode="jsx"
              theme="monokai"
              value={code}
              onChange={(newCode) => setCode(newCode)}
              name="codeEditor"
              editorProps={{ $blockScrolling: true }}
              fontSize={14}
              showPrintMargin={true}
              showGutter={true}
              highlightActiveLine={true}
              setOptions={{
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true,
                enableSnippets: false,
                showLineNumbers: true,
                tabSize: 2,
              }}
              readOnly={true} // Nicht editierbar
            />
          </div>
        </div>
        <div className="col-lg-4 d-flex justify-content-center">
          <div className="form-group smartphone-container">
            <div
              className="p-3"
              style={{ width: "290px", height: "590px", overflow: "auto" }}
            >
              <ECommerce />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ArrayEditor;
