import React from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCardText,
} from "mdb-react-ui-kit";

function ArrayScenario() {
  return (
    <MDBCard>
      <MDBCardHeader>E-Commerce System</MDBCardHeader>
      <MDBCardBody>
        <MDBCardText>
          Sie sind für die Qualitätssicherung bei einem Softwareentwickler
          zuständig, der eine E-Commerce- Plattform entwickelt. Eine der
          wichtigsten Funktionen dieser Plattform ist der Produktfilter, mit dem
          Kunden Produkte nach verschiedenen Attributen wie Marke, Farbe und
          Preisklasse filtern können. Angesichts der Vielfalt der angebotenen
          Produkte ist es entscheidend, dass diese Funktion einwandfrei
          funktioniert. Ihre Aufgabe ist es, die Robustheit dieser Funktion
          durch Anwendung von Orthogonalen-Array-Tests (OAT) sicherzustellen.
          <br />
          <br />
          <i>
            Nachfolgend haben Sie Einsicht in die Anwendung und den Quellcode
            der Filterklasse.
          </i>
        </MDBCardText>
      </MDBCardBody>
    </MDBCard>
  );
}

export default ArrayScenario;
