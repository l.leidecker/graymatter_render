import { MDBCard, MDBCardBody, MDBCol, MDBRow, MDBBtn } from "mdb-react-ui-kit";
import { Link } from "react-router-dom";

import matrixImg from "../../assets/images/matrix.png";
import medalZeroImg from "../../assets/images/medal0.png";
import medalFirstImg from "../../assets/images/medal1.png";
import medalSecondImg from "../../assets/images/medal2.png";
import medalThirdImg from "../../assets/images/medal3.png";

function MatrixUnitCard({ progress }) {
  // Accept progress prop
  let medalImage;
  switch (progress) {
    case 1:
      medalImage = medalThirdImg;
      break;
    case 2:
      medalImage = medalSecondImg;
      break;
    case 3:
      medalImage = medalFirstImg;
      break;
    default:
      medalImage = medalZeroImg; // Default image for progress 0 or undefined
  }

  return (
    <MDBCard className="mb-4 mx-auto" style={{ maxWidth: "800px" }}>
      <MDBCardBody>
        <MDBRow className="align-items-center">
          {/* Abbildung links */}
          <MDBCol className="text-center col-3 d-none d-md-block">
            <img
              src={matrixImg}
              alt="Matrix"
              className="img-fluid d-block mx-auto"
              style={{ maxWidth: "60%" }}
            />
          </MDBCol>

          {/* Text */}
          <MDBCol className="col-8" md="6">
            <h4 className="mb-3">Matrix-Test</h4>
            <p>
              Einführung eines matrixbasierten Variablenmanagements für die
              Test-Abdeckung.
            </p>
            {/* Button */}
            <Link to="/matrix">
              <MDBBtn color="success">Starten</MDBBtn>
            </Link>
          </MDBCol>

          {/* Medaille rechts */}
          <MDBCol md="3" className="text-center col-4">
            <img
              src={medalImage} // Use medalImage based on progress
              alt="Medal"
              className="img-fluid d-block mx-auto"
              style={{ maxWidth: "60%" }}
            />
            <p className="mt-2">
              {progress === 0 ? "Ausstehend" : "Bestanden"}
            </p>
          </MDBCol>
        </MDBRow>
      </MDBCardBody>
    </MDBCard>
  );
}

export default MatrixUnitCard;
