import React, { useState } from "react";
import {
  MDBContainer,
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarToggler,
  MDBIcon,
  MDBNavbarNav,
  MDBNavbarItem,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBCollapse,
} from "mdb-react-ui-kit";
import { Link } from "react-router-dom";

import grayMatterIcon from "../assets/images/grayMatter_icon.png";

export default function Navbar() {
  const [showBasic, setShowBasic] = useState(false);

  // Handle Logout
  const handleLogout = () => {
    localStorage.removeItem("jwtToken"); // JWT token entfernen
    window.location = "/login"; // Zur Login Page weiterleiten
  };

  const navBrandStyle = {
    display: "flex",
    alignItems: "center",
    textDecoration: "none",
  };

  return (
    <MDBNavbar
      expand="lg"
      light
      bgColor="light"
      style={{ position: "relative" }}
      className="mb-3"
    >
      <MDBContainer fluid>
        <MDBNavbarBrand>
          <Link to="/">
            <img
              src={grayMatterIcon}
              height="30"
              alt="grayMatter Icon"
              loading="lazy"
            />
          </Link>
        </MDBNavbarBrand>

        <MDBNavbarToggler
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
          onClick={() => setShowBasic(!showBasic)}
        >
          <MDBIcon icon="bars" fas />
        </MDBNavbarToggler>

        <MDBCollapse navbar show={showBasic}>
          <MDBNavbarNav className="mr-auto">
            <MDBNavbarItem>
              <Link
                to="/dashboard"
                className="nav-link"
                activeClassName="active"
              >
                Dashboard
              </Link>
            </MDBNavbarItem>
            <MDBNavbarItem>
              <Link to="/units" className="nav-link">
                Lerneinheiten
              </Link>
            </MDBNavbarItem>
          </MDBNavbarNav>

          <div style={{ position: "absolute", right: 0, marginRight: "15px" }}>
            <MDBNavbarNav>
              <MDBNavbarItem>
                <MDBDropdown>
                  <MDBDropdownToggle tag="a" className="nav-link" role="button">
                    <MDBIcon icon="user-circle" size="xl" />
                  </MDBDropdownToggle>
                  <MDBDropdownMenu>
                    <MDBDropdownItem className="ms-2">
                      <Link to="/settings" className="nav-link">
                        <MDBIcon fas icon="wrench" className="me-2" />
                        Einstellungen
                      </Link>
                    </MDBDropdownItem>
                    {/* Keep the logout logic as it was, or if you have a route, you can add it similarly */}
                    <MDBDropdownItem link onClick={handleLogout}>
                      <MDBIcon fas icon="sign-out-alt" className="me-2" />
                      Abmelden
                    </MDBDropdownItem>
                  </MDBDropdownMenu>
                </MDBDropdown>
              </MDBNavbarItem>
            </MDBNavbarNav>
          </div>
        </MDBCollapse>
      </MDBContainer>
    </MDBNavbar>
  );
}
