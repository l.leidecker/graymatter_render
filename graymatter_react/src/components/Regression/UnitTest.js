/* global mocha, chai */

import React, { useState } from "react";
import { MDBBtn, MDBRow, MDBCol, MDBIcon } from "mdb-react-ui-kit";
import AceEditor from "react-ace";

import "ace-builds/src-noconflict/mode-jsx";
import "ace-builds/src-noconflict/theme-monokai";

import "./UnitTest.css";

// Import inventory functions from Webservice.js
import {
  addItemService,
  updateItemService,
  deleteItemService,
  getInventoryService,
  resetInventoryService,
} from "./Webservice";

// Für das schreiben von Assertions
const { expect } = window.chai;

function UnitTest(props) {
  const {
    lastTestSuccessful,
    setLastTestSuccessful,
    inventoryCheck,
    setInventoryCheck,
  } = props;

  const initialCode = `const automatedTest = () => {
    inventoryAPI.addItem('Webcam', 10)
      .then(() => inventoryAPI.getInventory())
      .then((inventory) => {
        expect(inventory['Webcam']).to.equal(10);
        return inventoryAPI.addItem('Gamepad', 5);
      })
      .then(() => inventoryAPI.getInventory())
      .then((inventory) => {
        expect(inventory['Gamepad']).to.equal(5);
        return inventoryAPI.addItem('Webcam', 5);
      })
      .then(() => inventoryAPI.getInventory())
      .then((inventory) => {
        expect(inventory['Webcam']).to.equal(15);
        done(); // Ende des Tests
      })
      .catch((error) => {
        console.error("An error occurred in the test:", error);
        done(error); 
      });
  };
  
  automatedTest();
  `;
  const [code, setCode] = useState(initialCode);

  const [testError, setTestError] = useState(null); // State to handle test errors

  const [isRunning, setIsRunning] = useState(false); // Status ob Mocha Test noch läuft

  const logData = () => {
    console.log("Letzter Test war erfolgreich: ", lastTestSuccessful);
    console.log("Inventar beinhaltet die gewünschten Items: ", inventoryCheck);
  };

  // Function to reset the editor
  const resetEditor = () => {
    resetInventoryService(); // Reset Inventar
    setCode(initialCode); // Reset AceEditor
    props.handleResetInventory(); // Triggers State in Inventory.js
  };

  let mochaInitialized = false;

  const runCode = () => {
    setIsRunning(true); // Enable isRunning when the test starts

    if (!mochaInitialized) {
      mocha.setup({
        ui: "bdd",
        timeout: 10000,
      });
      mocha.cleanReferencesAfterRun(false);
      mochaInitialized = true;
    }

    mocha.suite.suites = []; // Clear previous Mocha tests if any

    describe("User Defined Tests", function () {
      it("User Test", function (done) {
        // Expose the necessary functions and variables to the user's code
        const inventoryAPI = {
          addItem: addItemService,
          updateItem: updateItemService,
          deleteItem: deleteItemService,
          getInventory: getInventoryService,
        };
        const { expect } = chai;

        // Dynamically create a new Function and pass the user's code into it
        const userFunction = new Function(
          "inventoryAPI",
          "expect",
          "done",
          code
        );

        // Execute the user function and handle promises correctly
        Promise.resolve()
          .then(() => {
            userFunction(inventoryAPI, expect, done);
          })
          .catch((error) => {
            console.error("An error occurred while running the code:", error);
            setTestError(error.toString()); // Update state with error message
            done(error); // Pass the error to Mocha
          });
      });
    });

    mocha.run((failures) => {
      setIsRunning(false); // Disable isRunning when the test finishes
      setLastTestSuccessful(failures === 0); // Checkt ob letzter Test erfolgreich war

      // Checkt ob Inventar der Vorgaben entspricht nach letztem Test-Run
      getInventoryService()
        .then((inventory) => {
          const isCorrectInventory =
            inventory["Webcam"] === 15 && inventory["Gamepad"] === 5;
          setInventoryCheck(isCorrectInventory);
        })
        .catch((error) => {
          console.error("Error checking inventory:", error);
          setInventoryCheck(false);
        });
    });
  };

  return (
    <div className="mt-2 mb-4">
      <h4>Aufgabe 2: Unit-Test</h4>
      <p>
        <i>
          Programmieren Sie nachfolgend einen Mocha & Chai Unit-Test zur
          Validierung des Webservices für das SOA-basierte
          Inventarverwaltungssystem. Nutzen Sie hierzu die Webservice API. Der
          Test soll:
          <ul>
            <li>"10 Webcam" hinzufügen und überprüfen</li>
            <li>"5 Gamepad" hinzufügen und überprüfen</li>
            <li>Weitere "5 Webcam" hinzufügen und überprüfen</li>
          </ul>
        </i>
      </p>
      <MDBRow>
        <MDBCol lg="7" className="mb-3">
          <AceEditor
            style={{
              width: "100%",
              height: "460px",
              overflow: "auto",
            }}
            mode="jsx"
            theme="monokai"
            value={code}
            onChange={(newCode) => setCode(newCode)}
            name="codeEditor"
            editorProps={{ $blockScrolling: true }}
            fontSize={14}
            showPrintMargin={true}
            showGutter={true}
            highlightActiveLine={true}
            setOptions={{
              enableBasicAutocompletion: true,
              enableLiveAutocompletion: true,
              enableSnippets: false,
              showLineNumbers: true,
              tabSize: 2,
            }}
            readOnly={false}
          />
        </MDBCol>
        <MDBCol lg="5" md="12">
          <div className="">
            {/* Run button */}
            <MDBBtn
              color="success"
              className="me-2"
              onClick={runCode}
              disabled={isRunning}
            >
              <MDBIcon fas icon="play" className="me-2" />
              Ausführen
            </MDBBtn>

            {/* Reset button */}
            <MDBBtn
              outline
              color="danger"
              onClick={resetEditor}
              disabled={isRunning}
            >
              <MDBIcon fas icon="stop" className="me-2" />
              Zurücksetzen
            </MDBBtn>
          </div>
          <div id="mocha"></div>
          <div id="error-output"></div>
        </MDBCol>
      </MDBRow>
      <div>
        {testError && ( // Display error message if there is an error
          <div className="alert alert-danger" role="alert">
            Test Error: {testError}
          </div>
        )}
      </div>
    </div>
  );
}

export default UnitTest;
