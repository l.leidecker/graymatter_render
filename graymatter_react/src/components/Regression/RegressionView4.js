import React, { useState } from "react";

import RegressionScenario from "./RegressionScenario";
import RegressionEditor from "./RegressionEditor";
import UnitTest from "./UnitTest";

function RegressionView4(props) {
  // State und Funktion zum handlen der Reset Funktion
  const [resetInventoryFlag, setResetInventoryFlag] = useState(false);

  const handleResetInventory = () => {
    setResetInventoryFlag((prevFlag) => !prevFlag);
  };

  return (
    <div className={props.className}>
      <RegressionScenario />
      <RegressionEditor resetInventoryFlag={resetInventoryFlag} />
      <UnitTest
        handleResetInventory={handleResetInventory}
        lastTestSuccessful={props.lastTestSuccessful}
        setLastTestSuccessful={props.setLastTestSuccessful}
        inventoryCheck={props.inventoryCheck}
        setInventoryCheck={props.setInventoryCheck}
      />
    </div>
  );
}

export default RegressionView4;
