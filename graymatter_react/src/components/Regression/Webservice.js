// Webservice.js

let inventory = {
  Tastatur: 5,
  Maus: 10,
  Monitor: 3,
  Headset: 7,
};

export const resetInventoryService = () => {
  inventory = {
    Tastatur: 5,
    Maus: 10,
    Monitor: 3,
    Headset: 7,
  };
};

export const getInventoryService = async () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(inventory);
    }, 1000);
  });
};

export const addItemService = async (itemName, itemCount) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      inventory[itemName] = (inventory[itemName] || 0) + parseInt(itemCount);
      resolve(inventory);
    }, 1000);
  });
};

export const updateItemService = async (itemName, itemCount) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (inventory.hasOwnProperty(itemName)) {
        inventory[itemName] = parseInt(itemCount);
        resolve(inventory);
      } else {
        reject(new Error("Item does not exist"));
      }
    }, 1000);
  });
};

export const deleteItemService = async (itemName) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (inventory.hasOwnProperty(itemName)) {
        delete inventory[itemName];
        resolve(inventory);
      } else {
        reject(new Error("Item does not exist"));
      }
    }, 1000);
  });
};
