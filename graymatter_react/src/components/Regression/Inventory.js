import React, { useState, useEffect } from "react";
import {
  addItemService,
  updateItemService,
  deleteItemService,
  getInventoryService,
} from "./Webservice";

function Inventory(props) {
  const [inventory, setInventory] = useState({});
  const [itemName, setItemName] = useState("");
  const [itemCount, setItemCount] = useState(0);
  const [alertVisible, setAlertVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  // Fetch initial inventory when component mounts
  useEffect(() => {
    const fetchInventory = async () => {
      setIsLoading(true);
      const initialInventory = await getInventoryService();
      setInventory(initialInventory);
      setIsLoading(false);
    };
    fetchInventory();
  }, [props.resetInventoryFlag]);

  const addItem = async () => {
    if (!itemName) {
      return;
    }

    setIsLoading(true);
    const updatedInventory = await addItemService(itemName, itemCount);
    setInventory(updatedInventory);
    setIsLoading(false);
    setAlertVisible(false);
  };

  const updateItem = async () => {
    setIsLoading(true);
    try {
      const updatedInventory = await updateItemService(itemName, itemCount);
      setInventory(updatedInventory);
      setAlertVisible(false);
    } catch (e) {
      setAlertVisible(true);
    }
    setIsLoading(false);
  };

  const deleteItem = async () => {
    setIsLoading(true);
    try {
      const updatedInventory = await deleteItemService(itemName);
      setInventory(updatedInventory);
      setAlertVisible(false);
    } catch (e) {
      setAlertVisible(true);
    }
    setIsLoading(false);
  };

  return (
    <div className="container">
      <h3 className="text-center my-4">Inventar Manager</h3>
      <div className="mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Artikel"
          value={itemName}
          onChange={(e) => setItemName(e.target.value)}
          required
        />
      </div>
      <div className="mb-3">
        <input
          type="number"
          className="form-control"
          placeholder="Item Count"
          value={itemCount}
          onChange={(e) => setItemCount(e.target.value)}
        />
      </div>
      <div>
        <button
          className="btn btn-primary btn-sm me-2"
          onClick={addItem}
          disabled={isLoading}
        >
          Hinzufügen
        </button>
        <button
          className="btn btn-primary btn-sm me-2"
          onClick={updateItem}
          disabled={isLoading}
        >
          Update
        </button>
        <button
          className="btn btn-danger btn-sm mt-2"
          onClick={deleteItem}
          disabled={isLoading}
        >
          Löschen
        </button>
      </div>

      {alertVisible && (
        <div className="alert alert-danger mt-3">Item does not exist!</div>
      )}

      <div className="mt-4">
        <h5>Aktueller Bestand:</h5>
        <ul className="list-group">
          {Object.keys(inventory).map((item) => (
            <li className="list-group-item" key={item}>
              {item}: {inventory[item]}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default Inventory;
