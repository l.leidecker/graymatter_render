import React, { useState, useEffect } from "react";
import AceEditor from "react-ace";

import "ace-builds/src-noconflict/mode-jsx";
import "ace-builds/src-noconflict/theme-monokai";

// Abruf des Codes von BibLogin.js für den Code Editor mittels Webpack
// eslint-disable-next-line import/no-webpack-loader-syntax
import inventoryContent from "!!raw-loader!./Inventory.js";
import Inventory from "./Inventory";

import "./RegressionEditor.css";

function RegressionEditor(props) {
  const [code, setCode] = useState(inventoryContent);
  const [RenderedComponent, setRenderedComponent] = useState(null);

  useEffect(() => {
    try {
      const func = new Function("React", code);
      setRenderedComponent(() => () => func(React));
    } catch (error) {
      console.error(error);
    }
  }, [code]);

  return (
    <div className="container my-4">
      <div className="row">
        <div className="col-lg-8">
          <div className="form-group mt-2">
            <AceEditor
              style={{
                width: "100%",
                height: "650px",
                overflow: "auto",
              }}
              mode="jsx"
              theme="monokai"
              value={code}
              onChange={(newCode) => setCode(newCode)}
              name="codeEditor"
              editorProps={{ $blockScrolling: true }}
              fontSize={14}
              showPrintMargin={true}
              showGutter={true}
              highlightActiveLine={true}
              setOptions={{
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true,
                enableSnippets: false,
                showLineNumbers: true,
                tabSize: 2,
              }}
              readOnly={true} // Nicht editierbar
            />
          </div>
        </div>
        <div className="col-lg-4 d-flex justify-content-center">
          <div className="form-group smartphone-container">
            <div
              className="p-3"
              style={{ width: "290px", height: "590px", overflow: "auto" }}
            >
              <Inventory resetInventoryFlag={props.resetInventoryFlag} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RegressionEditor;
