import React, { useEffect, useState } from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
} from "mdb-react-ui-kit";
import kursImage from "../../assets/images/kurs.png";
import "./RegressionView3.css";

function RegressionView3(props) {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    setLoaded(true);
  }, []);

  return (
    <div className={props.className}>
      <div className="row">
        {/* Left column */}
        <div className="col-12 col-lg-8">
          <div className="d-flex flex-column align-items-start">
            {/* Card 1 */}
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Mocha & Chai Unit-Test</MDBCardTitle>
                <MDBCardText>
                  <i>Mocha und Chai</i> sind zwei beliebte Bibliotheken, die
                  häufig für Unit-Tests in JavaScript-Anwendungen verwendet
                  werden. <i>Mocha</i> ist ein flexibles{" "}
                  <strong>Testframework</strong>, mit dem Entwickler asynchrone
                  und synchrone Tests definieren können. Es bietet eine
                  umfangreiche API für die Strukturierung von Tests und die
                  Definition von Testbedingungen. <i>Chai</i> hingegen ist eine
                  Assertion-Bibliothek, die es Entwicklern ermöglicht,{" "}
                  <strong>Behauptungen über den Zustand einer Anwendung</strong>{" "}
                  in lesbarer Form auszudrücken. Zusammen bieten Mocha und Chai
                  ein mächtiges Werkzeug-Set zum Schreiben von gut
                  strukturierten Unit-Tests. Dieser Ansatz ermöglicht es
                  Entwicklern, das Verhalten ihrer Anwendungen im Detail zu
                  überprüfen und die Zuverlässigkeit des Codes im Rahmen von
                  <i> Regressionstests</i> sicherzustellen.
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>

            {/* Card 2 */}
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards 0.5s" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>
                  Hinweise für das Schreiben eines Unit-Tests mit Mocha & Chai
                </MDBCardTitle>
                <MDBCardText>
                  <ul>
                    <li>
                      <strong>Promises und deren Verkettung:</strong> JavaScript
                      arbeitet oft asynchron, besonders bei Datenabrufen oder
                      API-Aufrufen. Bei Tests, die aufeinanderfolgende
                      asynchrone Schritte beinhalten, nutzen Sie
                      <strong> .then()</strong>-Methoden, um diese Schritte zu
                      verketten.
                    </li>
                    <li>
                      <strong>Mocha’s done Callback:</strong> Bei asynchronen
                      Tests, nutzen Sie Mochas done-Funktion, um das Framework
                      zu informieren, wenn Ihr Test abgeschlossen ist.
                      Beispielsweise in einem Test, der eine Datenbankabfrage
                      beinhaltet, rufen Sie <strong>done()</strong> am Ende des
                      letzten then()-Blocks auf.
                    </li>
                    <li>
                      <strong>Assertions mit Chai: </strong>Nutzen Sie Chais
                      expect oder should für Assertions. Diese ermöglichen es
                      Ihnen, Annahmen über Werte in Ihrem Code zu formulieren.
                      Zum Beispiel, <strong>expect(result).to.be.true</strong>{" "}
                      überprüft, ob result true ist.
                    </li>
                    <li>
                      <strong>Fehlerhandling: </strong>Implementieren Sie eine
                      Fehlerbehandlung in Ihren Tests. Verwenden Sie
                      <strong>.catch()</strong>-Blöcke in Promises, um Fehler zu
                      fangen und angemessen darauf zu reagieren.
                    </li>
                    <li>
                      <strong>Konkrete API-Interaktionen: </strong>Üben Sie das
                      Schreiben von Tests, die mit spezifischen Funktionen Ihrer
                      API interagieren. Beispielsweise könnten Sie in einem Test
                      überprüfen, ob das Hinzufügen eines Eintrags in einer
                      Datenbank-API den erwarteten Eintrag korrekt hinzufügt.
                    </li>
                  </ul>
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
          </div>
        </div>
        {/* Right column */}
        <div className="col-12 col-lg-4 d-none d-lg-flex justify-content-center align-items-center">
          <img src={kursImage} alt="Kurs" className="img-fluid" />
        </div>
      </div>
    </div>
  );
}

export default RegressionView3;
