import React, { useEffect, useState } from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
} from "mdb-react-ui-kit";
import kursImage from "../../assets/images/kurs.png";
import "./RegressionView1.css";

function RegressionView1(props) {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    setLoaded(true);
  }, []);

  return (
    <div className={props.className}>
      <div className="row">
        {/* Left column */}
        <div className="col-12 col-lg-8">
          <div className="d-flex flex-column align-items-start">
            {/* Card 1 */}
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Definition</MDBCardTitle>
                <MDBCardText>
                  <i>Regressionstests</i> sind Tests, die immer dann
                  durchgeführt werden, wenn{" "}
                  <strong>Änderungen an der Software </strong>
                  vorgenommen werden. Das Ziel ist es, zu überprüfen, ob die
                  Änderungen wie geplant funktionieren. Außerdem soll
                  sichergestellt werden, dass die Teile der Software, die
                  unverändert bleiben, durch die Änderungen nicht beeinträchtigt
                  werden. Fehler, die in unveränderten Teilen der Software
                  auftreten, werden als <strong>Regressionsfehler</strong>{" "}
                  bezeichnet. Regressionstest werden meist{" "}
                  <strong>automatisiert </strong>
                  ausgeführt.
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>

            {/* Card 2 */}
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards 0.5s" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Vorteile</MDBCardTitle>
                <MDBCardText>
                  <ul>
                    <li>
                      <strong>Qualitätssicherung:</strong> Erhöht die
                      Zuverlässigkeit der Software, indem sichergestellt wird,
                      dass Änderungen keine bestehenden Funktionen
                      beeinträchtigen.
                    </li>
                    <li>
                      <strong>Risikominimierung:</strong> Identifiziert
                      frühzeitig Probleme, die durch neue Updates entstehen
                      können.
                    </li>
                  </ul>
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>

            {/* Card 3 */}
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards 1s" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Anwendungsfälle</MDBCardTitle>
                <MDBCardText>
                  <ul>
                    <li>
                      <strong>Automatisierte Unit-Tests:</strong> Für
                      Regressionstests sind automatisierte Unit-Tests von großer
                      Bedeutung, da sie kontinuierlich und effizient Änderungen
                      überprüfen können.
                    </li>
                    <li>
                      <strong>Fortlaufende Entwicklung:</strong> Wichtig in
                      agilen Entwicklungsumgebungen, wo regelmäßige Updates und
                      Iterationen an der Tagesordnung sind.
                    </li>
                  </ul>
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
          </div>
        </div>
        {/* Right column */}
        <div className="col-12 col-lg-4 d-none d-lg-flex justify-content-center align-items-center">
          <img src={kursImage} alt="Kurs" className="img-fluid" />
        </div>
      </div>
    </div>
  );
}

export default RegressionView1;
