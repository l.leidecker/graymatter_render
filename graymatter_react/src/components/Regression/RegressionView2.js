import React, { useState } from "react";
import { MDBRadio, MDBCard, MDBCardBody } from "mdb-react-ui-kit";

import questionBook from "../../assets/images/question_book.png";

function RegressionView2(props) {
  // States der ausgewählten Antworten
  const {
    selectedQuestion1,
    setSelectedQuestion1,
    selectedQuestion2,
    setSelectedQuestion2,
    selectedQuestion3,
    setSelectedQuestion3,
    selectedQuestion4,
    setSelectedQuestion4,
  } = props;

  // Handler für veränderte Auswahl
  const handleQuestion1Change = (value) => {
    setSelectedQuestion1(value);
  };

  const handleQuestion2Change = (value) => {
    setSelectedQuestion2(value);
  };

  const handleQuestion3Change = (value) => {
    setSelectedQuestion3(value);
  };

  const handleQuestion4Change = (value) => {
    setSelectedQuestion4(value);
  };

  return (
    <div className={props.className}>
      <div className="row">
        {/* Linke Spalte */}
        <div className="col-12 col-lg-8">
          <MDBCard className="mb-3">
            <MDBCardBody>
              {/* Frage 1 */}
              <p>
                <strong>Was ist das Hauptziel von Regressionstests?</strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question1"
                  id="radio1A"
                  label="A) Die Leistung der Software zu verbessern."
                  onChange={() => handleQuestion1Change("A")}
                  checked={selectedQuestion1 === "A"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1B"
                  label="B) Neue Features zu implementieren."
                  onChange={() => handleQuestion1Change("B")}
                  checked={selectedQuestion1 === "B"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1C"
                  label="C) Zu überprüfen, ob Änderungen die bestehende Funktionalität beeinträchtigen"
                  onChange={() => handleQuestion1Change("C")}
                  checked={selectedQuestion1 === "C"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1D"
                  label="D) Alle Bugs in der Software zu finden."
                  onChange={() => handleQuestion1Change("D")}
                  checked={selectedQuestion1 === "D"}
                />
              </div>

              {/* Frage 2 */}
              <p>
                <strong>
                  In welchem Szenario sind Regressionstests besonders kritisch?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question2"
                  id="radio2A"
                  label="A) Beim ersten Release einer Software."
                  onChange={() => handleQuestion2Change("A")}
                  checked={selectedQuestion2 === "A"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2B"
                  label="B) Bei der Implementierung von Benutzeroberflächenänderungen."
                  onChange={() => handleQuestion2Change("B")}
                  checked={selectedQuestion2 === "B"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2C"
                  label="C) Nach der Integration neuer Softwaremodule"
                  onChange={() => handleQuestion2Change("C")}
                  checked={selectedQuestion2 === "C"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2D"
                  label="D) Bei der Änderung von Dokumentationen."
                  onChange={() => handleQuestion2Change("D")}
                  checked={selectedQuestion2 === "D"}
                />
              </div>

              {/* Frage 3 */}
              <p>
                <strong>
                  Warum sind automatisierte Unit-Tests in Regressionstests
                  wichtig?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question3"
                  id="radio3A"
                  label="A) Sie verbessern die Benutzererfahrung."
                  onChange={() => handleQuestion3Change("A")}
                  checked={selectedQuestion3 === "A"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3B"
                  label="B) Sie ermöglichen eine kontinuierliche und effiziente Überprüfung von Änderungen"
                  onChange={() => handleQuestion3Change("B")}
                  checked={selectedQuestion3 === "B"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3C"
                  label="C) Sie helfen dabei, spezifische Funktionen isoliert zu testen."
                  onChange={() => handleQuestion3Change("C")}
                  checked={selectedQuestion3 === "C"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3D"
                  label="D) Sie reduzieren die Entwicklungszeit für neue Features."
                  onChange={() => handleQuestion3Change("D")}
                  checked={selectedQuestion3 === "D"}
                />
              </div>

              {/* Frage 4 */}
              <p>
                <strong>
                  Welchen Vorteil bieten Regressionstests in agilen
                  Entwicklungsumgebungen?
                </strong>
              </p>
              <div>
                <MDBRadio
                  name="question4"
                  id="radio4A"
                  label="A) Sie ermöglichen eine schnellere Markteinführung."
                  onChange={() => handleQuestion4Change("A")}
                  checked={selectedQuestion4 === "A"}
                />
                <MDBRadio
                  name="question4"
                  id="radio4B"
                  label="B) Sie erhöhen die Anzahl der Testfälle."
                  onChange={() => handleQuestion4Change("B")}
                  checked={selectedQuestion4 === "B"}
                />
                <MDBRadio
                  name="question4"
                  id="radio4C"
                  label="C) Sie gewährleisten eine angemessene Testabdeckung trotz regelmäßiger Updates"
                  onChange={() => handleQuestion4Change("C")}
                  checked={selectedQuestion4 === "C"}
                />
                <MDBRadio
                  name="question4"
                  id="radio4D"
                  label="D) Sie ersetzen die Notwendigkeit von Endnutzertests."
                  onChange={() => handleQuestion4Change("D")}
                  checked={selectedQuestion4 === "D"}
                />
              </div>
            </MDBCardBody>
          </MDBCard>
        </div>
        {/* Rechte Spalte */}
        <div className="col-12 col-lg-4 d-none d-lg-flex justify-content-center align-items-center">
          <img src={questionBook} alt="Buch" className="img-fluid" />
        </div>
      </div>
    </div>
  );
}

export default RegressionView2;
