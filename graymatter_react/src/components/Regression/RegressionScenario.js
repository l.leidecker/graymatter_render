import React from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCardText,
} from "mdb-react-ui-kit";

function RegressionScenario() {
  return (
    <MDBCard>
      <MDBCardHeader>SOA Inventarverwaltungssystem</MDBCardHeader>
      <MDBCardBody>
        <MDBCardText>
          Sie testen ein Inventarverwaltungssystem für den Einzelhandel, das auf
          der Grundlage einer serviceorientierten Architektur (SOA) entwickelt
          wurde und sich bei der Verarbeitung seiner Transaktionen in hohem Maße
          auf einen Webservice stützt. Kürzlich wurde ein Upgrade des
          Webservices durchgeführt.
          <br />
          <br />
          <i>
            Nachfolgend haben Sie Einsicht in die Anwendung und den Quellcode
            der Inventarklasse. Die Implementierungsdetails des Webservices sind
            ihnen verborgen.
          </i>
        </MDBCardText>
      </MDBCardBody>
    </MDBCard>
  );
}

export default RegressionScenario;
