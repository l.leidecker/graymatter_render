import React from "react";

function MatrixView2(props) {
  return (
    <div className={props.className}>
      <h4>View 2</h4>
      {/* rest of the component content */}
    </div>
  );
}

export default MatrixView2;
