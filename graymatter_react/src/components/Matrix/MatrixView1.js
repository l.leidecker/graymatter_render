import React, { useEffect, useState } from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
} from "mdb-react-ui-kit";
import kursImage from "../../assets/images/kurs.png";
import "./MatrixView1.css";

function MatrixView1(props) {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    setLoaded(true);
  }, []);

  return (
    <div className={props.className}>
      <div className="row">
        {/* Left column */}
        <div className="col-12 col-lg-8">
          <div className="d-flex flex-column align-items-start">
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Card 1</MDBCardTitle>
                <MDBCardText>This is the content of Card 1.</MDBCardText>
              </MDBCardBody>
            </MDBCard>
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards 0.5s" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Card 2</MDBCardTitle>
                <MDBCardText>This is the content of Card 2.</MDBCardText>
              </MDBCardBody>
            </MDBCard>
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards 1s" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Card 3</MDBCardTitle>
                <MDBCardText>This is the content of Card 3.</MDBCardText>
              </MDBCardBody>
            </MDBCard>
          </div>
        </div>
        {/* Right column */}
        <div className="col-12 col-lg-4 d-none d-lg-flex justify-content-center align-items-center">
          <img src={kursImage} alt="Kurs" className="img-fluid" />
        </div>
      </div>
    </div>
  );
}

export default MatrixView1;
