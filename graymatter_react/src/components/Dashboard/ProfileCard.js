import React from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardText,
  MDBCardTitle,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBIcon,
  MDBProgress,
  MDBProgressBar,
} from "mdb-react-ui-kit";

import { useNavigate } from "react-router-dom";

// Importing profile images
import absolvent from "../../assets/images/profile/absolvent.png";
import alien from "../../assets/images/profile/alien.png";
import amira from "../../assets/images/profile/amira.png";
import ammelie from "../../assets/images/profile/ammelie.png";
import anna from "../../assets/images/profile/anna.png";
import babsi from "../../assets/images/profile/babsi.png";
import burak from "../../assets/images/profile/burak.png";
import defaultAvatar from "../../assets/images/profile/default.png";
import dennis from "../../assets/images/profile/dennis.png";
import fabian from "../../assets/images/profile/fabian.png";
import herbert from "../../assets/images/profile/herbert.png";
import hero from "../../assets/images/profile/hero.png";
import lena from "../../assets/images/profile/lena.png";
import lisa from "../../assets/images/profile/lisa.png";
import maike from "../../assets/images/profile/maike.png";
import mark from "../../assets/images/profile/mark.png";
import otto from "../../assets/images/profile/otto.png";
import roboter from "../../assets/images/profile/roboter.png";
import transzendenz from "../../assets/images/profile/transzendenz.png";
import vincent from "../../assets/images/profile/vincent.png";

function ProfileCard({ personalData, allUserData }) {
  // Navigation to /settings
  const navigate = useNavigate();

  // Handler for the settings redirect
  const goToSettings = () => {
    navigate("/settings");
  };

  // Calculate user level
  const userLevel = personalData
    ? personalData.progress_theory +
      personalData.progress_matrix +
      personalData.progress_array +
      personalData.progress_pattern +
      personalData.progress_regression
    : 0;

  // Updated avatar images object
  const avatarImages = {
    absolvent,
    alien,
    amira,
    ammelie,
    anna,
    babsi,
    burak,
    default: defaultAvatar,
    dennis,
    fabian,
    herbert,
    hero,
    lena,
    lisa,
    maike,
    mark,
    otto,
    roboter,
    transzendenz,
    vincent,
  };

  const userAvatar = personalData
    ? avatarImages[personalData.profile_pic] || defaultAvatar
    : defaultAvatar;

  // Calculate user rank
  const sortedUserData = allUserData
    .map((user) => ({
      ...user,
      totalLevel:
        user.progress_theory +
        user.progress_matrix +
        user.progress_array +
        user.progress_pattern +
        user.progress_regression,
    }))
    .filter((user) => user.totalLevel > 0)
    .sort((a, b) => b.totalLevel - a.totalLevel);
  const userRank =
    sortedUserData.findIndex((user) => user.id === personalData.id) + 1;

  // Calculate the progress percentage
  const maxLevel = 15;
  const progressPercentage =
    userLevel > 0 ? Math.floor((userLevel / maxLevel) * 100) : 0;

  return (
    <MDBCard className="mb-3" style={{ height: "260px" }}>
      <MDBCardBody>
        <MDBRow>
          {/* Profile Image */}
          <MDBCol
            className="col-4 d-flex align-items-center"
            style={{ height: "calc(100% - 40px)" }}
          >
            <img
              src={userAvatar}
              alt="Profile"
              className="img-fluid rounded-circle d-block mx-auto"
              style={{ maxWidth: "130px" }}
            />
          </MDBCol>
          <MDBCol className="col-8">
            {/* Username */}
            <MDBCardTitle>
              <h3>@{personalData.user_name || "username"}</h3>
            </MDBCardTitle>
            {/* Text Content */}
            <MDBCardText className="mt-3">
              <p>
                <strong>
                  <MDBIcon
                    size="lg"
                    fas
                    icon="angle-double-up"
                    className="me-2"
                  />
                  Level: {userLevel || 0}
                </strong>
              </p>
              <p>
                <strong>
                  <MDBIcon size="lg" fab icon="hackerrank" className="me-2" />
                  Rang: {userRank > 0 ? userRank : 0}
                </strong>
              </p>
              <br />
            </MDBCardText>
          </MDBCol>
        </MDBRow>

        {/* Outlined Button on the top right */}
        <MDBBtn
          outline
          color="primary"
          className="position-absolute top-0 end-0 mt-3 me-3"
          onClick={goToSettings}
        >
          <MDBIcon fas icon="wrench" />
        </MDBBtn>
        {/* Progress Bar at the bottom */}
        <p className="ms-2">
          <i>Gesamtfortschritt</i>
        </p>
        <MDBProgress
          height="20"
          style={{
            position: "absolute",
            bottom: "40px",
            left: "20px",
            right: "20px",
          }}
        >
          <MDBProgressBar
            bgColor="success"
            width={progressPercentage}
            valuemin={0}
            valuemax={100}
          >
            {progressPercentage}%
          </MDBProgressBar>
        </MDBProgress>
      </MDBCardBody>
    </MDBCard>
  );
}

export default ProfileCard;
