import {
  MDBCard,
  MDBCardBody,
  MDBCardText,
  MDBCardHeader,
  MDBCol,
  MDBRow,
  MDBBtn,
} from "mdb-react-ui-kit";
import { Link } from "react-router-dom";

import theoryImg from "../../assets/images/matrix.png";
import medalZeroImg from "../../assets/images/medal0.png";
import medalFirstImg from "../../assets/images/medal1.png";
import medalSecondImg from "../../assets/images/medal2.png";
import medalThirdImg from "../../assets/images/medal3.png";

function MatrixRecommendedCard(props) {
  // Determine which medal image to use
  let medalImg;
  switch (props.personalData.progress_matrix) {
    case 3:
      medalImg = medalFirstImg;
      break;
    case 2:
      medalImg = medalSecondImg;
      break;
    case 1:
      medalImg = medalThirdImg;
      break;
    default:
      medalImg = medalZeroImg;
  }

  return (
    <MDBCard className="mb-3" style={{ minHeight: "260px" }}>
      <MDBCardHeader>Empfohlene Lerneinheit</MDBCardHeader>
      <MDBCardBody>
        <MDBRow className="align-items-center">
          {/* Abbildung links */}
          <MDBCol className="text-center col-3 d-none d-md-block">
            <img
              src={theoryImg}
              alt="Theorie"
              className="img-fluid d-block mx-auto"
              style={{ maxWidth: "60%" }}
            />
          </MDBCol>

          {/* Text */}
          <MDBCol className="col-8" md="6">
            <h4 className="mb-3">Matrix-Test</h4>
            <p>
              Einführung eines matrixbasierten Variablenmanagements für die
              Test-Abdeckung.{" "}
            </p>
            {/* Button */}
            <Link to="/matrix">
              <MDBBtn color="success">Starten</MDBBtn>
            </Link>
          </MDBCol>

          {/* Medaille rechts */}
          <MDBCol md="3" className="text-center col-4">
            <img
              src={medalImg}
              alt="Medal"
              className="img-fluid d-block mx-auto"
              style={{ maxWidth: "60%" }}
            />
            <p className="mt-2">
              {props.personalData.progress_matrix === 0
                ? "Ausstehend"
                : "Bestanden"}
            </p>
          </MDBCol>
        </MDBRow>
      </MDBCardBody>
    </MDBCard>
  );
}

export default MatrixRecommendedCard;
