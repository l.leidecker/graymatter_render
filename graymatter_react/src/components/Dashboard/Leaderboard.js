import React from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBRow,
  MDBCol,
} from "mdb-react-ui-kit";

// Importing new profile images
import absolvent from "../../assets/images/profile/absolvent.png";
import alien from "../../assets/images/profile/alien.png";
import amira from "../../assets/images/profile/amira.png";
import ammelie from "../../assets/images/profile/ammelie.png";
import anna from "../../assets/images/profile/anna.png";
import babsi from "../../assets/images/profile/babsi.png";
import burak from "../../assets/images/profile/burak.png";
import defaultAvatar from "../../assets/images/profile/default.png";
import dennis from "../../assets/images/profile/dennis.png";
import fabian from "../../assets/images/profile/fabian.png";
import herbert from "../../assets/images/profile/herbert.png";
import hero from "../../assets/images/profile/hero.png";
import lena from "../../assets/images/profile/lena.png";
import lisa from "../../assets/images/profile/lisa.png";
import maike from "../../assets/images/profile/maike.png";
import mark from "../../assets/images/profile/mark.png";
import otto from "../../assets/images/profile/otto.png";
import roboter from "../../assets/images/profile/roboter.png";
import transzendenz from "../../assets/images/profile/transzendenz.png";
import vincent from "../../assets/images/profile/vincent.png";

import "./Leaderboard.css";

function Leaderboard({ allUserData, personalData }) {
  // Function to calculate the level
  const calculateLevel = (user) => {
    return (
      user.progress_theory +
      user.progress_matrix +
      user.progress_array +
      user.progress_pattern +
      user.progress_regression
    );
  };

  // Updated function to get the avatar image
  const getAvatarImage = (profilePic) => {
    const avatarImages = {
      absolvent,
      alien,
      amira,
      ammelie,
      anna,
      babsi,
      burak,
      default: defaultAvatar,
      dennis,
      fabian,
      herbert,
      hero,
      lena,
      lisa,
      maike,
      mark,
      otto,
      roboter,
      transzendenz,
      vincent,
    };

    return avatarImages[profilePic] || defaultAvatar;
  };

  // Process the user data for the leaderboard
  const leaderboardData = allUserData
    .map((user) => ({ ...user, level: calculateLevel(user) }))
    .filter((user) => user.level > 0)
    .sort((a, b) => b.level - a.level)
    .map((user, index) => ({
      ...user,
      rank: index + 1,
      profilePicture: getAvatarImage(user.profile_pic),
    }));

  return (
    <MDBCard style={{ height: "calc(2 * 260px + 15px)" }}>
      <MDBCardHeader>Rangliste (der letzten 7 Tage)</MDBCardHeader>
      <MDBCardBody style={{ overflowY: "auto", height: "calc(2 * 240px)" }}>
        {leaderboardData.map((entry, idx, arr) => (
          <MDBRow
            key={entry.rank}
            className={`align-items-center mb-2 position-relative ${
              entry.id === personalData.id ? "highlight-row" : ""
            }`} // Highlight if current user
            style={
              idx !== arr.length - 1
                ? { paddingBottom: "5px", marginBottom: "5px" }
                : {}
            }
          >
            <MDBCol className="col-2">
              <b>{entry.rank}</b>
            </MDBCol>
            <MDBCol className="col-2">
              <img
                src={entry.profilePicture}
                alt={`Profile of ${entry.user_name}`}
                style={{ width: "40px", borderRadius: "50%" }}
              />
            </MDBCol>
            <MDBCol className="col-8 d-flex justify-content-between">
              <strong>{entry.user_name}</strong>
              <span>(Level {entry.level})</span>
            </MDBCol>

            {idx !== arr.length - 1 && (
              <div
                style={{
                  position: "absolute",
                  bottom: "-2.5px",
                  left: "0",
                  right: "0",
                  borderBottom: "1px solid #e0e0e0",
                }}
              />
            )}
          </MDBRow>
        ))}
      </MDBCardBody>
    </MDBCard>
  );
}

export default Leaderboard;
