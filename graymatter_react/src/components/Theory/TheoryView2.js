import React, { useState } from "react";
import { MDBRadio, MDBCard, MDBCardBody } from "mdb-react-ui-kit";

import questionBook from "../../assets/images/question_book.png";

function TheoryView2(props) {
  // States and Handler from Theory.js
  const {
    selectedQuestion1,
    setSelectedQuestion1,
    selectedQuestion2,
    setSelectedQuestion2,
    selectedQuestion3,
    setSelectedQuestion3,
    selectedQuestion4,
    setSelectedQuestion4,
    handleQuestionChange,
  } = props;

  return (
    <div className={props.className}>
      <div className="row">
        {/* Linke Spalte */}
        <div className="col-12 col-lg-8">
          <MDBCard className="mb-3">
            <MDBCardBody>
              {/* Frage 1 */}
              <p>
                <strong>
                  Welcher Aspekt des Gray-Box Testing macht es besonders
                  geeignet für Integrationstests in komplexen
                  Softwareumgebungen?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question1"
                  id="radio1A"
                  label="A) Die Fokussierung ausschließlich auf externe Aspekte der Software"
                  onChange={() => handleQuestionChange(1, "A")}
                  checked={selectedQuestion1 === "A"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1B"
                  label="B) Die vollständige Einsicht in die interne Struktur der Software"
                  onChange={() => handleQuestionChange(1, "B")}
                  checked={selectedQuestion1 === "B"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1C"
                  label="C) Die Kombination von Funktionalitätsbewertung und Prüfung der internen Struktur"
                  onChange={() => handleQuestionChange(1, "C")}
                  checked={selectedQuestion1 === "C"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1D"
                  label="D) Die ausschließliche Nutzung automatisierter Testverfahren"
                  onChange={() => handleQuestionChange(1, "D")}
                  checked={selectedQuestion1 === "D"}
                />
              </div>

              {/* Frage 2 */}
              <p>
                <strong>
                  Was unterscheidet Gray-Box Testing grundsätzlich von reinem
                  Black-Box Testing?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question2"
                  id="radio2A"
                  label="A) Gray-Box Testing berücksichtigt nur die externe Funktionalität der Software."
                  onChange={() => handleQuestionChange(2, "A")}
                  checked={selectedQuestion2 === "A"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2B"
                  label="B) Gray-Box Testing erfordert ein tiefgehendes Verständnis aller internen Abläufe der Software."
                  onChange={() => handleQuestionChange(2, "B")}
                  checked={selectedQuestion2 === "B"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2C"
                  label="C) Gray-Box Testing kombiniert externe Funktionalitätsbewertungen mit einem begrenzten Einblick in interne Abläufe."
                  onChange={() => handleQuestionChange(2, "C")}
                  checked={selectedQuestion2 === "C"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2D"
                  label="D) Gray-Box Testing wird ausschließlich für End-to-End-Tests verwendet."
                  onChange={() => handleQuestionChange(2, "D")}
                  checked={selectedQuestion2 === "D"}
                />
              </div>

              {/* Frage 3 */}
              <p>
                <strong>
                  Bei welcher Testmethode liegt der Schwerpunkt darauf,
                  paarweise Kombinationen von unabhängigen Daten oder Entitäten
                  als Testeingabeparameter zu verwenden, um eine umfassende
                  Testabdeckung zu erreichen?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question3"
                  id="radio3A"
                  label="A) Regressionstests"
                  onChange={() => handleQuestionChange(3, "A")}
                  checked={selectedQuestion3 === "A"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3B"
                  label="B) Orthogonale-Array-Tests"
                  onChange={() => handleQuestionChange(3, "B")}
                  checked={selectedQuestion3 === "B"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3C"
                  label="C) Pattern-Tests"
                  onChange={() => handleQuestionChange(3, "C")}
                  checked={selectedQuestion3 === "C"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3D"
                  label="D) Matrix-Testing"
                  onChange={() => handleQuestionChange(3, "D")}
                  checked={selectedQuestion3 === "D"}
                />
              </div>

              {/* Frage 4 */}
              <p>
                <strong>
                  Wenn ein Tester wiederkehrende Muster innerhalb eines
                  Softwaresystems identifizieren möchte, um potenzielle
                  Fehlerquellen zu erkennen, welche Testmethode wird am ehesten
                  verwendet?
                </strong>
              </p>
              <div>
                <MDBRadio
                  name="question4"
                  id="radio4A"
                  label="A) Regressionstests"
                  onChange={() => handleQuestionChange(4, "A")}
                  checked={selectedQuestion4 === "A"}
                />
                <MDBRadio
                  name="question4"
                  id="radio4B"
                  label="B) Orthogonale-Array-Tests"
                  onChange={() => handleQuestionChange(4, "B")}
                  checked={selectedQuestion4 === "B"}
                />
                <MDBRadio
                  name="question4"
                  id="radio4C"
                  label="C) Pattern-Tests"
                  onChange={() => handleQuestionChange(4, "C")}
                  checked={selectedQuestion4 === "C"}
                />
                <MDBRadio
                  name="question4"
                  id="radio4D"
                  label="D) Matrix-Testing"
                  onChange={() => handleQuestionChange(4, "D")}
                  checked={selectedQuestion4 === "D"}
                />
              </div>
            </MDBCardBody>
          </MDBCard>
        </div>

        {/* Rechte Spalte */}
        <div className="col-12 col-lg-4 d-none d-lg-flex justify-content-center align-items-center">
          <img src={questionBook} alt="Buch" className="img-fluid" />
        </div>
      </div>
    </div>
  );
}

export default TheoryView2;
