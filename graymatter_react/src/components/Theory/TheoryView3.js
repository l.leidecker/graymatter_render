import React from "react";

function TheoryView3(props) {
  return (
    <div className={props.className}>
      <h4>Einführung in das Gray-Box Testing</h4>
      <p>
        <i>
          Schauen Sie sich das Kapitel zu Gray-Box Testing an (4:45 bis 6:52).
        </i>
      </p>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <iframe
          width="800"
          height="450"
          src="https://www.youtube-nocookie.com/embed/CPVMbZYeiF4?si=CeB7FvFe9XYAM6DL&amp;start=285;end=402"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowfullscreen
        ></iframe>
      </div>
    </div>
  );
}

export default TheoryView3;
