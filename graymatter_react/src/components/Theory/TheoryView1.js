import React, { useEffect, useState } from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
} from "mdb-react-ui-kit";
import kursImage from "../../assets/images/kurs.png";
import "./TheoryView1.css";

function TheoryView1(props) {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    setLoaded(true);
  }, []);

  return (
    <div className={props.className}>
      <div className="row">
        {/* Left column */}
        <div className="col-12 col-lg-8">
          <div className="d-flex flex-column align-items-start">
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Definition</MDBCardTitle>
                <MDBCardText>
                  Gray-Box Testing ist eine Testart, die eine Zwischenform
                  zwischen Black-Box- und White-Box-Testing darstellt. Es
                  kombiniert Elemente beider Ansätze, um ein Gleichgewicht
                  zwischen Funktionalitätsbewertung und Prüfung der internen
                  Struktur zu erreichen. Im Gegensatz zu reinen Black-Box-Tests,
                  die sich ausschließlich auf die externen Aspekte der Software
                  konzentrieren, und White-Box-Tests, die eine vollständige
                  Einsicht in die interne Struktur der Software erfordern,
                  benötigt Gray-Box Testing nur ein begrenztes Verständnis der
                  internen Abläufe.
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards 0.5s" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Vorteile</MDBCardTitle>
                <MDBCardText>
                  <ul>
                    <li>
                      <strong>Kombinierter Fokus:</strong> Gray-Box Testing
                      vereint die Vorteile von Black-Box- und White-Box-Tests,
                      indem es sowohl die Funktionalität als auch die interne
                      Struktur der Software ins Visier nimmt.
                    </li>
                    <li>
                      <strong>Effizienz in Integrationstests:</strong> Diese
                      Testmethode ist besonders effektiv bei Integrationstests,
                      da sie es ermöglicht, sowohl Schnittstellen als auch
                      dahinterliegende Logik zu bewerten.
                    </li>
                    <li>
                      <strong>Effizienz in SOA:</strong> Durch die begrenzte,
                      aber gezielte Einsicht in die Code-Struktur eignet sich
                      Gray-Box Testing besonders für Tests von Webanwendungen
                      und Serviceorientierten Architekturen (SOA).
                    </li>
                  </ul>
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
            <MDBCard
              className="mb-4 w-100"
              style={{
                animation: loaded ? "fade-in-down 1s forwards 1s" : "none",
                opacity: 0,
              }}
            >
              <MDBCardBody>
                <MDBCardTitle>Testmethoden</MDBCardTitle>
                <MDBCardText>
                  <ul>
                    <li>
                      <strong>Regressionstests:</strong> Diese Tests werden
                      durchgeführt, um zu überprüfen, ob Änderungen an der
                      Software wie geplant funktionieren und ob unveränderte
                      Teile durch die Änderungen beeinträchtigt werden.
                    </li>
                    <li>
                      <strong>Orthogonale-Array-Tests:</strong> Diese Methode
                      kombiniert unabhängige Daten oder Entitäten paarweise als
                      Testeingabeparameter. Sie ermöglicht eine effiziente
                      Überprüfung aller Variablenkombinationen und erhöht die
                      Testabdeckung.
                    </li>
                    <li>
                      <strong>Pattern-Tests:</strong> Diese Tests konzentrieren
                      sich darauf, wiederkehrende Muster innerhalb des
                      Softwaresystems zu identifizieren, die potenziell zu
                      Fehlern führen können.
                    </li>
                    <li>
                      <strong>Matrix-Testing:</strong>Dieser Ansatz bewertet
                      Software systematisch, indem er sich auf ihre Variablen
                      und Methoden konzentriert. Es bewertet jede Variable und
                      Methode basierend auf technischem Risiko.
                    </li>
                  </ul>
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
          </div>
        </div>
        {/* Right column */}
        <div className="col-12 col-lg-4 d-none d-lg-flex justify-content-center align-items-center">
          <img src={kursImage} alt="Kurs" className="img-fluid" />
        </div>
      </div>
    </div>
  );
}

export default TheoryView1;
