import React, { useState } from "react";
import { MDBRadio, MDBCard, MDBCardBody } from "mdb-react-ui-kit";

// Sie können hier Ihr eigenes Bild importieren
import questionBook from "../../assets/images/question_book.png";

function TheoryView4(props) {
  // States and Handler from Theory.js
  const {
    selectedQuestion5,
    setSelectedQuestion5,
    selectedQuestion6,
    setSelectedQuestion6,
    selectedQuestion7,
    setSelectedQuestion7,
    handleQuestionChange,
  } = props;

  return (
    <div className={props.className}>
      <div className="row">
        {/* Linke Spalte */}
        <div className="col-12 col-lg-8">
          <MDBCard className="mb-3">
            <MDBCardBody>
              {/* Frage 1 */}
              <p>
                <strong>
                  Ein Softwareprojekt befindet sich in der frühen
                  Entwicklungsphase. Das Entwicklerteam möchte sicherstellen,
                  dass jede Zeile des Codes gemäß den Spezifikationen
                  funktioniert und dass es keine versteckten Fehler oder
                  Sicherheitslücken gibt. Welche Testart sollte für diese
                  Aufgabe verwendet werden?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question1"
                  id="radio1A"
                  label="A) Black-Box Testing"
                  onChange={() => handleQuestionChange(5, "A")}
                  checked={selectedQuestion5 === "A"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1B"
                  label="B) White-Box Testing"
                  onChange={() => handleQuestionChange(5, "B")}
                  checked={selectedQuestion5 === "B"}
                />
                <MDBRadio
                  name="question1"
                  id="radio1C"
                  label="C) Gray-Box Testing"
                  onChange={() => handleQuestionChange(5, "C")}
                  checked={selectedQuestion5 === "C"}
                />
              </div>

              {/* Frage 2 */}
              <p>
                <strong>
                  Ein Entwicklerteam arbeitet an einer serviceorientierten
                  Architektur (SOA), bei der mehrere Dienste miteinander
                  interagieren. Das Team möchte sicherstellen, dass die
                  Schnittstellen korrekt funktionieren und die Dienste wie
                  erwartet zusammenarbeiten. Welche Testmethode eignet sich am
                  besten, um sowohl die Schnittstellen als auch die
                  Interaktionen zwischen den Diensten zu testen?
                </strong>
              </p>
              <div className="mb-4">
                <MDBRadio
                  name="question2"
                  id="radio2A"
                  label="A) Black-Box Testing"
                  onChange={() => handleQuestionChange(6, "A")}
                  checked={selectedQuestion6 === "A"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2B"
                  label="B) White-Box Testing"
                  onChange={() => handleQuestionChange(6, "B")}
                  checked={selectedQuestion6 === "B"}
                />
                <MDBRadio
                  name="question2"
                  id="radio2C"
                  label="C) Gray-Box Testing"
                  onChange={() => handleQuestionChange(6, "C")}
                  checked={selectedQuestion6 === "C"}
                />
              </div>

              {/* Frage 3 */}
              <p>
                <strong>
                  Ein Team führt Endbenutzertests für eine neue Webanwendung
                  durch. Das Ziel ist es, die Anwendung aus der Perspektive des
                  Endbenutzers zu testen, ohne sich auf den internen Code oder
                  die Architektur zu konzentrieren. Welche Testart ist in diesem
                  Szenario am effektivsten?
                </strong>
              </p>
              <div>
                <MDBRadio
                  name="question3"
                  id="radio3A"
                  label="A) Black-Box Testing"
                  onChange={() => handleQuestionChange(7, "A")}
                  checked={selectedQuestion7 === "A"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3B"
                  label="B) White-Box Testing"
                  onChange={() => handleQuestionChange(7, "B")}
                  checked={selectedQuestion7 === "B"}
                />
                <MDBRadio
                  name="question3"
                  id="radio3C"
                  label="C) Gray-Box Testing"
                  onChange={() => handleQuestionChange(7, "C")}
                  checked={selectedQuestion7 === "C"}
                />
              </div>
            </MDBCardBody>
          </MDBCard>
        </div>

        {/* Rechte Spalte */}
        <div className="col-12 col-lg-4 d-none d-lg-flex justify-content-center align-items-center">
          <img src={questionBook} alt="Buch" className="img-fluid" />
        </div>
      </div>
    </div>
  );
}

export default TheoryView4;
