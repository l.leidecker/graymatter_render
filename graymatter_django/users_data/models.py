from django.contrib.auth.models import User
from django.db import models

class UserData(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE) 
    user_name = models.CharField(max_length=255) # Wird durch Programmierung bei Registrierung gesetzt
    profile_pic = models.CharField(max_length=255, default="default")
    progress_theory = models.IntegerField(default=0)
    progress_matrix = models.IntegerField(default=0)
    progress_array = models.IntegerField(default=0)
    progress_pattern = models.IntegerField(default=0)
    progress_regression = models.IntegerField(default=0)