# Generated by Django 4.2.5 on 2023-10-05 17:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UserData',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.IntegerField(unique=True)),
                ('user_name', models.CharField(max_length=255)),
                ('profile_pic', models.CharField(max_length=255)),
                ('progress_theory', models.IntegerField()),
                ('progress_matrix', models.IntegerField()),
                ('progress_array', models.IntegerField()),
                ('progress_pattern', models.IntegerField()),
                ('progress_regression', models.IntegerField()),
            ],
        ),
    ]
