from rest_framework import viewsets, status, permissions
from rest_framework.response import Response
from rest_framework.decorators import action
from .models import UserData
from .serializers import UserDataSerializer

class IsOwner(permissions.BasePermission):
    """
    Custom permission to only allow owners to edit their data.
    """

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user

class UserDataViewSet(viewsets.ModelViewSet):
    queryset = UserData.objects.all()
    serializer_class = UserDataSerializer
    http_method_names = ['get', 'put', 'patch', 'head', 'options']

    def get_permissions(self):
        if self.action in ['update', 'partial_update', 'me']:
            permission_classes = [permissions.IsAuthenticated, IsOwner]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]

    @action(detail=False, methods=['get', 'put', 'patch'])
    def me(self, request, *args, **kwargs):
        """
        Return or update the user data of the currently authenticated user.
        """
        print("Request method:", request.method)
        user_data = UserData.objects.filter(user=request.user).first()
        if not user_data:
            return Response({"detail": "Not found."}, status=status.HTTP_404_NOT_FOUND)
        if request.method in ['PUT', 'PATCH']:
            print("Data being serialized:", request.data)
            serializer = self.get_serializer(user_data, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        print("Request method:", request.method)
        print("No PUT or PATCH request")
        serializer = self.get_serializer(user_data)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        allowed_fields = ["profile_pic", "progress_theory", "progress_matrix", "progress_array", "progress_pattern", "progress_regression"]
        if any(field not in allowed_fields for field in request.data.keys()):
            return Response({"detail": "You can only update specific fields."}, status=status.HTTP_400_BAD_REQUEST)
        
        return super(UserDataViewSet, self).update(request, *args, **kwargs)
