from rest_framework import serializers
from djoser.serializers import UserCreateSerializer
from django.contrib.auth import get_user_model
from .models import UserData

class UserDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserData
        fields = '__all__'

    def update(self, instance, validated_data):
        # List of fields that can be updated
        allowed_fields = [
            "profile_pic", 
            "progress_theory", 
            "progress_matrix", 
            "progress_array", 
            "progress_pattern", 
            "progress_regression"
        ]

        for field in allowed_fields:
            if field in validated_data:
                setattr(instance, field, validated_data[field])

        instance.save()
        return instance

# Custom User Creation serializer
User = get_user_model()

class CustomUserCreateSerializer(UserCreateSerializer):
    class Meta(UserCreateSerializer.Meta):
        model = User

    def create(self, validated_data):
        # Use the original create method to create the user
        user = super(CustomUserCreateSerializer, self).create(validated_data)

        # Now, create the UserData entry for the new user
        UserData.objects.create(
            user=user,
            user_name=user.username,
            profile_pic="default",
            progress_theory=0,
            progress_matrix=0,
            progress_array=0,
            progress_pattern=0,
            progress_regression=0
        )

        return user
